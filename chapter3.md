<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_003{font-family:Times,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Times,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Times,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Times,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Calibri",serif;font-size:9.1px;color:rgb(38,37,38);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Calibri",serif;font-size:9.1px;color:rgb(38,37,38);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Times,serif;font-size:40.1px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Times,serif;font-size:40.1px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Times,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Times,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Times,serif;font-size:36.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_013{font-family:Times,serif;font-size:36.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_014{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_014{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_015{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_015{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_016{font-family:Times,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Times,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Times,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_017{font-family:Times,serif;font-size:18.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_018{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Times,serif;font-size:22.0px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Times,serif;font-size:22.0px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_020{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_020{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:24.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:24.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Times,serif;font-size:32.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Times,serif;font-size:32.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="chapter3/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:13.20px;top:8.72px" class="cls_003"><span class="cls_003">11e</span></div>
<div style="position:absolute;left:271.62px;top:11.92px" class="cls_002"><span class="cls_002">Database Systems</span></div>
<div style="position:absolute;left:160.13px;top:35.92px" class="cls_002"><span class="cls_002">Design, Implementation, and Management</span></div>
<div style="position:absolute;left:471.58px;top:279.60px" class="cls_004"><span class="cls_004">Coronel | Morris</span></div>
<div style="position:absolute;left:281.19px;top:350.48px" class="cls_005"><span class="cls_005">Chapter 3</span></div>
<div style="position:absolute;left:162.94px;top:407.60px" class="cls_005"><span class="cls_005">The Relational Database</span></div>
<div style="position:absolute;left:307.25px;top:455.60px" class="cls_005"><span class="cls_005">Model</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:197.25px;top:52.88px" class="cls_007"><span class="cls_007">Learning Objectives</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> In this chapter, one will learn:</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> That the relational database model offers a logical view</span></div>
<div style="position:absolute;left:94.95px;top:202.48px" class="cls_011"><span class="cls_011">of data</span></div>
<div style="position:absolute;left:75.55px;top:242.56px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> About the relational model’s basic component:</span></div>
<div style="position:absolute;left:94.95px;top:273.52px" class="cls_011"><span class="cls_011">relations</span></div>
<div style="position:absolute;left:75.55px;top:314.56px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> That relations are logical constructs composed of rows</span></div>
<div style="position:absolute;left:94.95px;top:345.52px" class="cls_011"><span class="cls_011">(tuples) and columns (attributes)</span></div>
<div style="position:absolute;left:75.55px;top:385.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> That relations are implemented as tables in a relational</span></div>
<div style="position:absolute;left:94.95px;top:416.56px" class="cls_011"><span class="cls_011">DBMS</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:705.70px;top:519.52px" class="cls_012"><span class="cls_012">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:197.25px;top:52.88px" class="cls_007"><span class="cls_007">Learning Objectives</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> In this chapter, one will learn:</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> About relational database operators, the data dictionary,</span></div>
<div style="position:absolute;left:94.95px;top:202.48px" class="cls_011"><span class="cls_011">and the system catalog</span></div>
<div style="position:absolute;left:75.55px;top:242.56px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> How data redundancy is handled in the relational</span></div>
<div style="position:absolute;left:94.95px;top:273.52px" class="cls_011"><span class="cls_011">database model</span></div>
<div style="position:absolute;left:75.55px;top:314.56px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Why indexing is important</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:705.70px;top:519.52px" class="cls_012"><span class="cls_012">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:169.41px;top:50.00px" class="cls_007"><span class="cls_007">A Logical View of Data</span></div>
<div style="position:absolute;left:57.80px;top:140.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Relational database model enables logical</span></div>
<div style="position:absolute;left:77.95px;top:174.56px" class="cls_009"><span class="cls_009">representation of the data and its relationships</span></div>
<div style="position:absolute;left:57.80px;top:216.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Logical simplicity yields simple and effective</span></div>
<div style="position:absolute;left:77.95px;top:250.64px" class="cls_009"><span class="cls_009">database design methodologies</span></div>
<div style="position:absolute;left:57.80px;top:292.64px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Facilitated by the creation of data relationships</span></div>
<div style="position:absolute;left:77.95px;top:326.48px" class="cls_009"><span class="cls_009">based on a logical construct called a relation</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:705.70px;top:519.52px" class="cls_012"><span class="cls_012">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:60.76px;top:33.84px" class="cls_013"><span class="cls_013">Table 3.1 - Characteristics of a Relational</span></div>
<div style="position:absolute;left:320.26px;top:76.80px" class="cls_013"><span class="cls_013">Table</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:705.70px;top:519.52px" class="cls_012"><span class="cls_012">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:318.87px;top:52.88px" class="cls_007"><span class="cls_007">Keys</span></div>
<div style="position:absolute;left:51.85px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Consist of one or more attributes that determine other</span></div>
<div style="position:absolute;left:72.00px;top:162.56px" class="cls_009"><span class="cls_009">attributes</span></div>
<div style="position:absolute;left:51.85px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Used to:</span></div>
<div style="position:absolute;left:75.60px;top:247.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Ensure that each row in a table is uniquely identifiable</span></div>
<div style="position:absolute;left:75.60px;top:287.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Establish relationships among tables and to ensure the</span></div>
<div style="position:absolute;left:95.05px;top:318.64px" class="cls_011"><span class="cls_011">integrity of the data</span></div>
<div style="position:absolute;left:51.85px;top:358.64px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Primary key (PK)</span><span class="cls_009">: Attribute or combination of</span></div>
<div style="position:absolute;left:72.00px;top:392.48px" class="cls_009"><span class="cls_009">attributes that uniquely identifies any given row</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:705.70px;top:519.52px" class="cls_012"><span class="cls_012">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:244.44px;top:52.88px" class="cls_007"><span class="cls_007">Determination</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> State in which knowing the value of one attribute</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">makes it possible to determine the value of another</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Is the basis for establishing the role of a key</span></div>
<div style="position:absolute;left:51.80px;top:247.52px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Based on the relationships among the attributes</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:705.70px;top:519.52px" class="cls_012"><span class="cls_012">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:247.81px;top:52.88px" class="cls_007"><span class="cls_007">Dependencies</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Functional dependence</span><span class="cls_009">: Value of one or more</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">attributes determines the value of one or more other</span></div>
<div style="position:absolute;left:71.95px;top:195.68px" class="cls_009"><span class="cls_009">attributes</span></div>
<div style="position:absolute;left:75.55px;top:238.48px" class="cls_010"><span class="cls_010">§</span><span class="cls_015"> Determinant</span><span class="cls_011">: Attribute whose value determines</span></div>
<div style="position:absolute;left:94.95px;top:269.68px" class="cls_011"><span class="cls_011">another</span></div>
<div style="position:absolute;left:75.55px;top:309.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_015"> Dependent</span><span class="cls_011">: Attribute whose value is determined by the</span></div>
<div style="position:absolute;left:94.95px;top:341.68px" class="cls_011"><span class="cls_011">other attribute</span></div>
<div style="position:absolute;left:51.80px;top:381.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Full functional dependence</span><span class="cls_009">: Entire collection of</span></div>
<div style="position:absolute;left:71.95px;top:414.56px" class="cls_009"><span class="cls_009">attributes in the determinant is necessary for the</span></div>
<div style="position:absolute;left:71.95px;top:448.64px" class="cls_009"><span class="cls_009">relationship</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:705.70px;top:519.52px" class="cls_012"><span class="cls_012">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:244.71px;top:52.88px" class="cls_007"><span class="cls_007">Types of Keys</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Composite key</span><span class="cls_009">: Key that is composed of more than</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">one attribute</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Key attribute</span><span class="cls_009">: Attribute that is a part of a key</span></div>
<div style="position:absolute;left:51.80px;top:247.52px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Entity integrity</span><span class="cls_009">: Condition in which each row in the</span></div>
<div style="position:absolute;left:71.95px;top:280.64px" class="cls_009"><span class="cls_009">table has its own unique identity</span></div>
<div style="position:absolute;left:75.55px;top:323.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> All of the values in the primary key must be unique</span></div>
<div style="position:absolute;left:75.55px;top:363.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> No key attribute in the primary key can contain a null</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:705.70px;top:519.52px" class="cls_012"><span class="cls_012">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:244.71px;top:52.88px" class="cls_007"><span class="cls_007">Types of Keys</span></div>
<div style="position:absolute;left:43.20px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Null</span><span class="cls_009">: Absence of any data value that could represent:</span></div>
<div style="position:absolute;left:79.20px;top:171.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> An unknown attribute value</span></div>
<div style="position:absolute;left:79.20px;top:211.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> A known, but missing, attribute value</span></div>
<div style="position:absolute;left:79.20px;top:251.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> A inapplicable condition</span></div>
<div style="position:absolute;left:43.20px;top:291.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Referential integrity</span><span class="cls_009">: Every reference to an entity</span></div>
<div style="position:absolute;left:70.20px;top:325.52px" class="cls_009"><span class="cls_009">instance by another entity instance is valid</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:62.58px;top:52.88px" class="cls_007"><span class="cls_007">Table 3.3 - Relational Database Keys</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.22px;top:519.52px" class="cls_012"><span class="cls_012">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:93.99px;top:33.60px" class="cls_016"><span class="cls_016">Figure 3.2 - An Example of a Simple</span></div>
<div style="position:absolute;left:215.50px;top:76.56px" class="cls_016"><span class="cls_016">Relational Database</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:240.50px;top:52.88px" class="cls_007"><span class="cls_007">Integrity Rules</span></div>
<div style="position:absolute;left:55.20px;top:169.20px" class="cls_017"><span class="cls_017">Entity Integrity</span></div>
<div style="position:absolute;left:373.20px;top:169.20px" class="cls_017"><span class="cls_017">Description</span></div>
<div style="position:absolute;left:55.20px;top:198.00px" class="cls_018"><span class="cls_018">Requirement</span></div>
<div style="position:absolute;left:376.70px;top:198.00px" class="cls_018"><span class="cls_018">All primary key entries are unique, and</span></div>
<div style="position:absolute;left:373.20px;top:219.84px" class="cls_018"><span class="cls_018">no part of a primary key may be null</span></div>
<div style="position:absolute;left:55.20px;top:270.00px" class="cls_018"><span class="cls_018">Purpose</span></div>
<div style="position:absolute;left:373.20px;top:270.00px" class="cls_018"><span class="cls_018">Each row will have a unique identity, and</span></div>
<div style="position:absolute;left:373.20px;top:291.84px" class="cls_018"><span class="cls_018">foreign key values can properly</span></div>
<div style="position:absolute;left:373.20px;top:313.92px" class="cls_018"><span class="cls_018">reference primary key values</span></div>
<div style="position:absolute;left:55.20px;top:378.24px" class="cls_018"><span class="cls_018">Example</span></div>
<div style="position:absolute;left:373.20px;top:378.24px" class="cls_018"><span class="cls_018">No invoice can have a duplicate number,</span></div>
<div style="position:absolute;left:373.20px;top:400.08px" class="cls_018"><span class="cls_018">nor it can be null</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:240.50px;top:52.88px" class="cls_007"><span class="cls_007">Integrity Rules</span></div>
<div style="position:absolute;left:55.20px;top:122.64px" class="cls_017"><span class="cls_017">Entity Integrity</span></div>
<div style="position:absolute;left:373.20px;top:122.64px" class="cls_017"><span class="cls_017">Description</span></div>
<div style="position:absolute;left:55.20px;top:151.44px" class="cls_018"><span class="cls_018">Requirement</span></div>
<div style="position:absolute;left:376.70px;top:151.44px" class="cls_018"><span class="cls_018">A foreign key may have either a null</span></div>
<div style="position:absolute;left:373.20px;top:173.28px" class="cls_018"><span class="cls_018">entry or a entry that matches a primary</span></div>
<div style="position:absolute;left:373.20px;top:195.36px" class="cls_018"><span class="cls_018">key value in a table to which it is related</span></div>
<div style="position:absolute;left:55.20px;top:245.04px" class="cls_018"><span class="cls_018">Purpose</span></div>
<div style="position:absolute;left:373.20px;top:245.04px" class="cls_018"><span class="cls_018">It is possible for an attribute not to have a</span></div>
<div style="position:absolute;left:373.20px;top:266.88px" class="cls_018"><span class="cls_018">corresponding value but it is impossible</span></div>
<div style="position:absolute;left:373.20px;top:288.96px" class="cls_018"><span class="cls_018">to have an invalid entry</span></div>
<div style="position:absolute;left:373.20px;top:331.92px" class="cls_018"><span class="cls_018">It is impossible to delete row in a table</span></div>
<div style="position:absolute;left:373.20px;top:353.04px" class="cls_018"><span class="cls_018">whose primary keys has mandatory</span></div>
<div style="position:absolute;left:373.20px;top:374.88px" class="cls_018"><span class="cls_018">matching foreign key values in another</span></div>
<div style="position:absolute;left:373.20px;top:396.96px" class="cls_018"><span class="cls_018">table</span></div>
<div style="position:absolute;left:55.20px;top:425.04px" class="cls_018"><span class="cls_018">Example</span></div>
<div style="position:absolute;left:373.20px;top:425.04px" class="cls_018"><span class="cls_018">It is impossible to have invalid sales</span></div>
<div style="position:absolute;left:373.20px;top:446.88px" class="cls_018"><span class="cls_018">representative number</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:80.49px;top:33.60px" class="cls_016"><span class="cls_016">Figure 3.3 - An Illustration of Integrity</span></div>
<div style="position:absolute;left:319.00px;top:76.56px" class="cls_016"><span class="cls_016">Rules</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:184.35px;top:52.88px" class="cls_007"><span class="cls_007">Ways to Handle Nulls</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Flags</span><span class="cls_009">: Special codes used to indicate the absence of</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">some value</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> NOT NULL constraint - Placed on a column to</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_009"><span class="cls_009">ensure that every row in the table has a value for that</span></div>
<div style="position:absolute;left:71.95px;top:271.52px" class="cls_009"><span class="cls_009">column</span></div>
<div style="position:absolute;left:51.80px;top:314.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> UNIQUE constraint - Restriction placed on a column</span></div>
<div style="position:absolute;left:71.95px;top:348.56px" class="cls_009"><span class="cls_009">to ensure that no duplicate values exist for that</span></div>
<div style="position:absolute;left:71.95px;top:381.68px" class="cls_009"><span class="cls_009">column</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:209.48px;top:52.88px" class="cls_007"><span class="cls_007">Relational Algebra</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Theoretical way of manipulating table contents using</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">relational operators</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Relvar</span><span class="cls_009">: Variable that holds a relation</span></div>
<div style="position:absolute;left:75.55px;top:247.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Heading contains the names of the attributes and the</span></div>
<div style="position:absolute;left:94.95px;top:278.56px" class="cls_011"><span class="cls_011">body contains the relation</span></div>
<div style="position:absolute;left:51.80px;top:318.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Relational operators have the property of closure</span></div>
<div style="position:absolute;left:75.55px;top:361.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_015"> Closure</span><span class="cls_011">: Use of relational algebra operators on existing</span></div>
<div style="position:absolute;left:94.95px;top:392.56px" class="cls_011"><span class="cls_011">relations produces new relations</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:163.31px;top:52.88px" class="cls_007"><span class="cls_007">Relational Set Operators</span></div>
<div style="position:absolute;left:92.28px;top:130.40px" class="cls_019"><span class="cls_019">Select (Restrict)</span></div>
<div style="position:absolute;left:91.36px;top:166.80px" class="cls_018"><span class="cls_018">• Unary operator that yields a horizontal subset of a table</span></div>
<div style="position:absolute;left:92.28px;top:210.80px" class="cls_019"><span class="cls_019">Project</span></div>
<div style="position:absolute;left:91.36px;top:247.20px" class="cls_018"><span class="cls_018">• Unary operator that yields a vertical subset of a table</span></div>
<div style="position:absolute;left:92.28px;top:291.20px" class="cls_019"><span class="cls_019">Union</span></div>
<div style="position:absolute;left:91.36px;top:327.60px" class="cls_018"><span class="cls_018">• Combines all rows from two tables, excluding duplicate rows</span></div>
<div style="position:absolute;left:91.36px;top:350.64px" class="cls_018"><span class="cls_018">•</span><span class="cls_020"> Union-compatible</span><span class="cls_018">: Tables share the same number of columns, and their</span></div>
<div style="position:absolute;left:100.36px;top:369.60px" class="cls_018"><span class="cls_018">corresponding columns share compatible domains</span></div>
<div style="position:absolute;left:92.28px;top:409.52px" class="cls_019"><span class="cls_019">Intersect</span></div>
<div style="position:absolute;left:91.36px;top:445.92px" class="cls_018"><span class="cls_018">• Yields only the rows that appear in both tables</span></div>
<div style="position:absolute;left:91.36px;top:468.96px" class="cls_018"><span class="cls_018">• Tables must be union-compatible to yield valid results</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:227.00px;top:55.20px" class="cls_016"><span class="cls_016">Figure 3.4 - Select</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:220.00px;top:55.20px" class="cls_016"><span class="cls_016">Figure 3.5 - Project</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:226.00px;top:55.20px" class="cls_016"><span class="cls_016">Figure 3.6 - Union</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:209.00px;top:55.20px" class="cls_016"><span class="cls_016">Figure 3.7 - Intersect</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:163.31px;top:52.88px" class="cls_007"><span class="cls_007">Relational Set Operators</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Difference</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Yields all rows in one table that are not found in the</span></div>
<div style="position:absolute;left:94.95px;top:202.48px" class="cls_011"><span class="cls_011">other table</span></div>
<div style="position:absolute;left:75.55px;top:242.56px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Tables must be union-compatible to yield valid results</span></div>
<div style="position:absolute;left:51.80px;top:282.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Product</span></div>
<div style="position:absolute;left:75.55px;top:325.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Yields all possible pairs of rows from two tables</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:163.31px;top:52.88px" class="cls_007"><span class="cls_007">Relational Set Operators</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Join</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Allows information to be intelligently combined from</span></div>
<div style="position:absolute;left:94.95px;top:202.48px" class="cls_011"><span class="cls_011">two or more tables</span></div>
<div style="position:absolute;left:51.80px;top:242.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Divide</span></div>
<div style="position:absolute;left:75.55px;top:285.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Uses one 2-column table as the dividend and one</span></div>
<div style="position:absolute;left:94.95px;top:316.48px" class="cls_011"><span class="cls_011">single-column table as the divisor</span></div>
<div style="position:absolute;left:75.55px;top:356.56px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Output is a single column that contains all values from</span></div>
<div style="position:absolute;left:94.95px;top:387.52px" class="cls_011"><span class="cls_011">the second column of the dividend that are associated</span></div>
<div style="position:absolute;left:94.95px;top:419.68px" class="cls_011"><span class="cls_011">with every row in the divisor</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:244.65px;top:52.88px" class="cls_007"><span class="cls_007">Types of Joins</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Natural join</span><span class="cls_009">: Links tables by selecting only the rows</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">with common values in their common attributes</span></div>
<div style="position:absolute;left:75.55px;top:204.64px" class="cls_010"><span class="cls_010">§</span><span class="cls_015"> Join columns</span><span class="cls_011">: Common columns</span></div>
<div style="position:absolute;left:51.80px;top:244.64px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Equijoin</span><span class="cls_009">: Links tables on the basis of an equality</span></div>
<div style="position:absolute;left:71.95px;top:278.48px" class="cls_009"><span class="cls_009">condition that compares specified columns of each</span></div>
<div style="position:absolute;left:71.95px;top:312.56px" class="cls_009"><span class="cls_009">table</span></div>
<div style="position:absolute;left:51.80px;top:354.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Theta join</span><span class="cls_009">: Extension of natural join, denoted by</span></div>
<div style="position:absolute;left:71.95px;top:388.64px" class="cls_009"><span class="cls_009">adding a theta subscript after the JOIN symbol</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:244.65px;top:52.88px" class="cls_007"><span class="cls_007">Types of Joins</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Inner join</span><span class="cls_009">: Only returns matched records from the</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">tables that are being joined</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Outer join</span><span class="cls_009">: Matched pairs are retained and</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_009"><span class="cls_009">unmatched values in the other table are left null</span></div>
<div style="position:absolute;left:75.55px;top:281.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_015"> Left outer join</span><span class="cls_011">: Yields all of the rows in the first table,</span></div>
<div style="position:absolute;left:94.95px;top:312.64px" class="cls_011"><span class="cls_011">including those that do not have a matching value in</span></div>
<div style="position:absolute;left:94.95px;top:343.60px" class="cls_011"><span class="cls_011">the second table</span></div>
<div style="position:absolute;left:75.55px;top:383.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_015"> Right outer join</span><span class="cls_011">: Yields all of the rows in the second</span></div>
<div style="position:absolute;left:94.95px;top:414.64px" class="cls_011"><span class="cls_011">table, including those that do not have matching values</span></div>
<div style="position:absolute;left:94.95px;top:446.56px" class="cls_011"><span class="cls_011">in the first table</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:194.32px;top:55.20px" class="cls_016"><span class="cls_016">Figure 3.8 - Difference</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:215.00px;top:55.20px" class="cls_016"><span class="cls_016">Figure 3.9 - Product</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:82.53px;top:33.60px" class="cls_016"><span class="cls_016">Figure 3.10 - Two Tables That Will Be</span></div>
<div style="position:absolute;left:170.50px;top:76.56px" class="cls_016"><span class="cls_016">Used in JOIN Illustrations</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:213.00px;top:55.20px" class="cls_016"><span class="cls_016">Figure 3.16 - Divide</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:69.50px;top:55.44px" class="cls_013"><span class="cls_013">Data Dictionary and the System Catalog</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Data dictionary</span><span class="cls_009">: Description of all tables in the</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">database created by the user and designer</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> System catalog</span><span class="cls_009">: System data dictionary that</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_009"><span class="cls_009">describes all objects within the database</span></div>
<div style="position:absolute;left:51.80px;top:280.64px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Homonyms and synonyms must be avoided to lessen</span></div>
<div style="position:absolute;left:71.95px;top:314.48px" class="cls_009"><span class="cls_009">confusion</span></div>
<div style="position:absolute;left:98.60px;top:357.60px" class="cls_021"><span class="cls_021">§</span><span class="cls_022"> Homonym</span><span class="cls_023">: Same name is used to label different</span></div>
<div style="position:absolute;left:115.85px;top:386.64px" class="cls_023"><span class="cls_023">attributes</span></div>
<div style="position:absolute;left:98.60px;top:423.60px" class="cls_021"><span class="cls_021">§</span><span class="cls_022"> Synonym</span><span class="cls_023">: Different names are used to describe the same</span></div>
<div style="position:absolute;left:115.85px;top:452.64px" class="cls_023"><span class="cls_023">attribute</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:106.50px;top:33.84px" class="cls_013"><span class="cls_013">Relationships within the Relational</span></div>
<div style="position:absolute;left:294.00px;top:76.80px" class="cls_013"><span class="cls_013">Database</span></div>
<div style="position:absolute;left:51.80px;top:134.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> 1:M relationship - Norm for relational databases</span></div>
<div style="position:absolute;left:51.80px;top:177.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> 1:1 relationship - One entity can be related to only</span></div>
<div style="position:absolute;left:71.95px;top:210.56px" class="cls_009"><span class="cls_009">one other entity and vice versa</span></div>
<div style="position:absolute;left:51.80px;top:253.52px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Many-to-many (M:N) relationship - Implemented by</span></div>
<div style="position:absolute;left:71.95px;top:286.64px" class="cls_009"><span class="cls_009">creating a new entity in 1:M relationships with the</span></div>
<div style="position:absolute;left:71.95px;top:320.48px" class="cls_009"><span class="cls_009">original entities</span></div>
<div style="position:absolute;left:75.55px;top:363.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_015"> Composite entity </span><span class="cls_011">(</span><span class="cls_015">Bridge </span><span class="cls_011">or </span><span class="cls_015">associative entity</span><span class="cls_011">):</span></div>
<div style="position:absolute;left:94.95px;top:394.48px" class="cls_011"><span class="cls_011">Helps avoid problems inherent to M:N relationships,</span></div>
<div style="position:absolute;left:94.95px;top:425.68px" class="cls_011"><span class="cls_011">includes the primary keys of tables to be linked</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:81.60px;top:38.56px" class="cls_024"><span class="cls_024">Figure 3.21 - The 1:1 Relationship between</span></div>
<div style="position:absolute;left:134.06px;top:76.48px" class="cls_024"><span class="cls_024">PROFESSOR and DEPARTMENT</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">33</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:127.00px;top:33.60px" class="cls_013"><span class="cls_013">Figure 3.26 - Changing the M:N</span></div>
<div style="position:absolute;left:77.58px;top:76.56px" class="cls_013"><span class="cls_013">Relationship to Two 1:M Relationships</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">34</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:78.82px;top:55.20px" class="cls_013"><span class="cls_013">Figure 3.27 - The Expanded ER Model</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">35</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:217.31px;top:52.88px" class="cls_007"><span class="cls_007">Data Redundancy</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Relational database facilitates control of data</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">redundancies through use of foreign keys</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> To be controlled except the following circumstances</span></div>
<div style="position:absolute;left:75.55px;top:247.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Data redundancy must be increased to make the</span></div>
<div style="position:absolute;left:94.95px;top:278.56px" class="cls_011"><span class="cls_011">database serve crucial information purposes</span></div>
<div style="position:absolute;left:75.55px;top:318.64px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Exists to preserve the historical accuracy of the data</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">36</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:62.82px;top:33.60px" class="cls_013"><span class="cls_013">Figure 3.30 - The Relational Diagram for</span></div>
<div style="position:absolute;left:207.00px;top:76.56px" class="cls_013"><span class="cls_013">the Invoicing System</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">37</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="chapter3/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:314.44px;top:52.88px" class="cls_007"><span class="cls_007">Index</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Orderly arrangement to logically access rows in a</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">table</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Index key</span><span class="cls_009">: Index’s reference point that leads to data</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_009"><span class="cls_009">location identified by the key</span></div>
<div style="position:absolute;left:51.80px;top:280.64px" class="cls_008"><span class="cls_008">§</span><span class="cls_014"> Unique index</span><span class="cls_009">: Index key can have only one pointer</span></div>
<div style="position:absolute;left:71.95px;top:314.48px" class="cls_009"><span class="cls_009">value associated with it</span></div>
<div style="position:absolute;left:51.80px;top:357.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Each index is associated with only one table</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.70px;top:519.52px" class="cls_012"><span class="cls_012">38</span></div>
</div>

</body>
</html>
