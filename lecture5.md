## Quiz      

1. The Advantages and Disadvantages 
    - Hierarchical Model 
        - Advantages 
            - Promotes data sharing 
            - Parent/child relationship promotesconceptual simplicity and data integrity 
            - Database security is provided and enforced by DBMS 
            - Efficient with 1:M relationships 
        - Disadvantages 
            - Requires knowledge of physicaldata storage characteristics 
            - Navigational system requires knowledge of hierarchical path 
            - Changes in structure require changes in all application programs 
            - Implementation limitations 
            - No data definition 
            - Lack of standards 
    - Network Model 
        - Advantages 
            - Conceptual simplicity 
            - Handles more relationship types 
            - Data access is flexible 
            - Data owner/member relationship promotes data integrity 
            - Conformance to standards 
            - Includes data definition language (DDL) and data manipulation language (DML) 
        - Disadvantages 
            - System complexity limits efficiency 
            - Navigational system yields complex implementation, application development, and management 
            - Structural changes require Structural changes require programs 
    - Relational Model 
    - Entity Relationship Model 
    - Object-Oriented Model 
    - NoSQL 

2. The External Model 
    - The external model is the end users’ view of the data environment 
        - The term end users refers to people who use the application programs to manipulate the data and generate information 
    - ER diagrams will be used to represent the external views 
        - A specific representation of an **external view** is known as an **external schema** 

3. The Conceptual Model 
    - The conceptual model represents a global view of the entire database by the entire organization 
        - conceptual schema, it is the basis for the identification and high-level description of the main data objects 
        - logical design refers to the task of creating a conceptual data model 
    - The advantages 
        - it provides a bird’s-eye (macro level) view of the data environment that is relatively easy to understand 
        - the conceptual model is independent of both software and hardware 

4. The Internal Model 
    - the internal model maps the conceptual model to the DBMS 
        - An internal schema depicts a specific representation of an internal model, using the database constructs supported by the chosen database 
        - When you can **change** the **internal** model **without affecting** the **conceptual** model, you have **logical independence** 
            - a **change** in **storage devices** or even a change in **operating systems** will **not affect** the internal model 


5. The Physical Model 
    - The physical model operates at the lowest level of abstraction 
        - describing the way data is saved on storage media such as magnetic, solid state, or optical media 
    - The physical model **requires the definition of both the physical storage devices and the (physical) access methods** required to reach the data within those storage devices, making it both **software and hardware dependent** 
    - the now-dominant relational model is **aimed largely at the logical level** rather than at the physical level 
        - it **does not require the physical-level details** common to its predecessor 
    - When you can **change** the physical model **without affecting** the internal model, you have **physical independence** 

































