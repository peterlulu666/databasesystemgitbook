## Quiz        

1. Data Modeling and Data Models
    - Data modeling
        - creating a specific data model for a determined problem domain
    - A data model
        - representation of more complex real-world data structures
        - supporting a specific problem domain
    - a model 
        - an abstraction of a more complex real-world object or event

2. The Importance of Data Models
    - help transform data into information
    - view data differently
    - different users
    - good database blueprint help people better understand and save cost

3. Data Model Basic Building Blocks
    - An entity is a person, place, thing, or event about which data will be collected and stored
    - An **attribute** is a characteristic of an **entity** 
        - a CUSTOMER **entity** would be described by **attributes** such as customer last name, customer first name, customer phone number, customer address, and customer credit limit 
        - **Attributes** are the equivalent of **fields** in file systems
    - relationship
        - One-to-many
            - very common
            - painter paintings
            - user comment
        - Many-to-many
            - not common
            - book author
            - student course
            - EMPLOYEE SKILL
        - One-to-one
            - least common
            - manager store
            - marage
            - people and ID
    - constraint
        - restriction placed on the data
        - ensure data integrity
            - integrity integrity 
            - referential integrity

4. Business Rules
    - From a database point of view, the collection of data becomes **meaningful** only when it reflects properly defined business rules 
    - A business rule is a **brief, precise, and unambiguous description of a policy, procedure, or principle** within a specific organization 
        - Business rules derived from a detailed description of an organization’s operations help to **create and enforce actions within that organization’s environment** 
        - Properly written business rules are used to define **entities, attributes, relationships, and constraints** 
            - Note that those business rules establish **entities**, **relationships**, and **constraints**. For example, the first two business rules establish two **entities** (CUSTOMER and INVOICE) and a 1:M **relationship** between those two entities. The third business rule establishes a **constraint** (no fewer than 10 people and no more than 30 people) and two entities (EMPLOYEE and TRAINING), and also implies a relationship between EMPLOYEE and TRAINING 

5. Discovering Business Rules 
    - The main **sources of business rules** are **company managers**, **policy makers**, **department managers**, and **written documentation** such as a company’s procedures, standards, and operations manual 
    - A faster and more direct source of business rules is **direct interviews with end user** 
        - because perceptions differ, **end users** are sometimes a **less reliable** source when it comes to specifying business rule 
    - The database **designer**’s job is to **reconcile** such differences and **verify** the results of the reconciliation to ensure that the business rules are appropri- ate and accurate 
    - The process of **identifying and documenting business rules** is essential to database design for several reason 
        - It helps to **standardize** the company’s view of data 
        - It can be a **communication tool** between users and designer 
        - It allows the **designer** to **understand the nature, role, and scope of the data** 
        - It allows the **designer** to **understand business processes** 
        - It allows the **designer** to **develop appropriate relationship participation rules and constraints** and to **create an accurate data model** 

6. Translating Business Rules into Data Model Component 
    - Business rules set the stage for the proper identification of entities, attributes, rela- tionships, and constraints 
        - As a general rule, a **noun** in a business rule will translate into an **entity** in the model, and a **verb** (active or passive) that associates the nouns will translate into a **relationship** among the entities 
    - To properly identify the type of **relationship**, you should consider that relationships are **bidirectional** 
        - How many instances of B are related to one instance of A 
        - How many instances of A are related to one instance of B 

7. Naming Convention 
    - **Entity** names should be **descriptive of the objects** in the business environment and use terminology that is **familiar** to the user 
    - An **attribute** name should also be **descriptive of the data** represented by that attribute 
    - The use of a **proper naming** convention will improve the data model’s ability to **facilitate communication** among the designer, application programmer, and the end user. In fact, a **proper naming** convention can go a long way toward making your model **self-documenting** 

8. Hierarchical and Network Model 
    - The **hierarchical model** was developed in the 1960s **to manage large amounts of data for complex manufacturing project** 
        - The model’s basic logical structure is represented by an **upside-down tree**. The hierarchical structure contains levels, or **segment** 
            - A **segment** is the equivalent of a file system’s record type 
        - The hierarchical model depicts a set of **one-to-many (1:M) relationships** between a parent and its children segments 
    - The **network model** was created to represent **complex data relationships more effectively** than the hierarchical model, to **improve** database **performance**, and to **impose** a database **standard**. In the network model, the user perceives the network database as a collection of records in 1:M relationships. However, unlike the hierarchical model, the network model allows a record to have **more than one parent** 
    - While the **network** database model is generally **not used** today, the definitions of **standard database concepts that emerged with the network model** are still **used** by modern data model 
        - The **schema** is the conceptual organization of the entire database as viewed by the database administrator 
        - The **subschema** defines the portion of the database “seen” by the application pro- grams that actually produce the desired information from the data within the database 
        - A **data manipulation language (DML)** defines the environment in which data can be managed and is used to work with the data in the database 
        - A **schema data definition language (DDL)** enables the database administrator to define the schema component 
    - Because of the disadvantages of the **hierarchical and network models**, they were largely **replaced by** the **relational** data model in the 1980s 
        - the disadvantages is that there is **proprietory code** in the hierarchical and network models 

9. The Relational Model 
    - The **relational model** represented a major breakthrough for both users and designers. To use an analogy, the relational model **produced an “automatic transmission” database to replace the “standard transmission” databases** that preceded it 
    - The relational model’s foundation is a **mathematical** concept known as a relation 
    - To avoid the complexity of abstract mathematical theory, you can think of a **relation** (sometimes called a table) as a two-dimensional structure composed of intersecting **rows** and **columns**. Each **row** in a relation is called a **tuple**. Each **column** represents an **attribute** 
    - The **relational model** also **describes a precise set of data manipulation constructs** based on advanced mathematical concept 
    - The relational data model is implemented through a very sophisticated **relational database management system (RDBMS)** 
        - The RDBMS performs the same basic functions provided by the **hierarchical** and **network** DBMS system 
        - make the relational data model **easier to understand and implement** 
        - Arguably the most important advantage of the RDBMS is its ability to **hide the complexities of the relational model from the user** 
    - SQL-based relational database application involves three parts: **a user interface**, **a set of tables stored in the database**, and **the SQL engine** 
        - The end-user interface 
            - the interface allows the end **user** to **interact** with the **data** 
        - A collection of tables stored in the database 
            - Each table is **independent** 
            - Rows in different tables are related by **common values** in **common attribute** 
        - SQL engine 
            - Largely hidden from the end user, **the SQL engine executes all queries** 

10. The Entity Relationship Model 
    - Peter Chen first introduced the ER data model in 1976 
    - the graphical representation of entities and their relationships in a database structure 
        - entity relationship diagram (ERD) 
            - uses graphical representations to model database components 
        - Entity 
            - Each **row** in the relational table is known as an **entity instance** or **entity occurrence** in the ER model 
        - Each entity consists of a set of **attributes** 
            - **describes particular characteristics** of the entity 
        - Relationships 
            - **Relationships** describe **associations** among data 
            - The ER model uses the term **connectivity** to label the **relationship** type 
    - the different types of relationships using three ER notation 
        - the original **Chen notation**, the **Crow’s Foot notation**, and the newer **class diagram notation**, which is part of the Unified Modeling Language (UML) 








































        




