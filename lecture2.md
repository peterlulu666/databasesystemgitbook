## Quiz        

1. Structural and Data Dependence
    - structural dependence
        - access to a file is dependent on its structure
        - all of the file system programs must be modified to conform to the new file structure
    - structural independence
        - change the file structure without affecting the application’s ability to access the data

2. Data redundancy
    - islands of information
        - scattered data **locations**
    - data stored in different **locations** will probably **not** be updated **consistently** 
    - the islands of information often contain different **versions** of the same data
    - Poor data security
    - Data inconsistency
    - Data-entry errors
    - Data integrity problems

3. Data Anomalies
    - Update anomalies
    - Insertion anomalies
    - Deletion anomalies
    - If you did not update, insert, and delete everywhere it exist, there is error

4. Database Systems
    - logically related data stored in a single logical data repository
        - physically distributed among multiple storage facilities
        - The database’s DBMS eliminate most of the file system’s data inconsistency, data anomaly, data dependence, and structural depen- dence problems
    - the current generation of DBMS software
        - stores not only the data structures but also the relationships between those structures and the access paths to those structures
        - takes care of defining, storing, and managing all required access paths to those components

5. The Database System Environment
    - organization of components that define and regulate the collection, storage, management, and use of data within a database environment
    - Hardware
    - Software
    - People
    - Procedures
    - Data
    - the different levels of complexity, cost-effective, tactically and strategically effective

6. DBMS Functions
    - Data dictionary management
        - The DBMS stores definitions of the data elements and their relationships (metadata) in a data dictionary
        - The DBMS uses the data dictionary to **look up** the required data component structures and relationships
        - any **changes** made in a database structure are **automatically** recorded in the data dictionary
        - use the key
    - Data storage management
        - Performance tuning relates to the activities that make the database **perform** more **efficiently** in terms of storage and access speed
    - Data transformation and presentation
        - make it conform to the user’s **logical** expectations
    - Security management
        - creates a security system that enforces user **security** and data privacy
        - the user access
    - Multiuser access control
        - sophisticated algorithms to ensure that **multiple users can access** the database concurrently without compromising its integrity
    - Backup and recovery management
        - the recovery of the database after a failure
    - Data integrity management
        - minimizing data redundancy and maximizing data consistency
    - Database access languages and application programming interfaces
        - A query language lets the user specify what must be done without having to specify how
        - Structured Query Language (SQL) is the de facto query language and data access standard supported by the majority of DBMS vendors
    - Database communication interfaces
        - accepts end-user requests via multiple, different network environments

7. The disadvantages of Database systems
    - Increased costs
        - maintaining
        - personnel required to operate and manage a database system
    - Management complexity
        - multiple sources 
        - security issues
    - Maintaining currency
        - You have to hire people to **updates** the database systems
        - You have to make sure the **security**
        - So there is **personnel training costs**
    - Vendor dependence
        - investment in technology and personnel training
    - Frequent upgrade/replacement cycles
        - cost money to upgrades the database systems
        - cost money to train people to use the new database systems

8. DATABASE CAREER OPPORTUNITIES

<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=ProgId content=Excel.Sheet>
<meta name=Generator content="Microsoft Excel 15">
<link rel=File-List
href="">
<style
 id="">
<!--table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
@page
	{margin:.75in .7in .75in .7in;
	mso-header-margin:.3in;
	mso-footer-margin:.3in;}
.font5
	{color:white;
	font-size:11.0pt;
	font-weight:700;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;}
.font6
	{color:#231F20;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial, sans-serif;
	mso-font-charset:0;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:black;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:"Times New Roman";
	mso-generic-font-family:auto;
	mso-font-charset:204;
	mso-number-format:General;
	text-align:left;
	vertical-align:top;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl65
	{mso-style-parent:style0;
	color:windowtext;
	font-size:11.0pt;
	font-weight:700;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:.5pt solid #808285;
	background:#2C6682;
	mso-pattern:black none;
	white-space:normal;}
.xl66
	{mso-style-parent:style0;
	color:windowtext;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:.5pt solid #808285;
	background:#FFCC7A;
	mso-pattern:black none;
	white-space:normal;}
.xl67
	{mso-style-parent:style0;
	color:windowtext;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:.5pt solid #808285;
	background:#FFE0AD;
	mso-pattern:black none;
	white-space:normal;}
-->
</style>
</head>

<body link=blue vlink=purple>
<!--[if !excel]>&nbsp;&nbsp;<![endif]-->
<!--The following information was generated by Microsoft Excel's Publish as Web
Page wizard.-->
<!--If the same item is republished from Excel, all information between the DIV
tags will be replaced.-->
<!----------------------------->
<!--START OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD -->
<!----------------------------->

<div
id="Database Systems Design, Implementation, and Management by Carlos Coronel Steven Morris (z-lib.org) (dragged)_32021"
align=center x:publishsource="Excel">

<table border=0 cellpadding=0 cellspacing=0 width=785 style='border-collapse:
 collapse;table-layout:fixed;width:588pt'>
 <col width=147 style='mso-width-source:userset;mso-width-alt:5632;width:110pt'>
 <col width=319 span=2 style='mso-width-source:userset;mso-width-alt:12236;
 width:239pt'>
 <tr height=21 style='mso-height-source:userset;height:16.25pt'>
  <td height=21 class=xl65 width=147 style='height:16.25pt;width:110pt'><font
  class="font5">JOB TITLE</font></td>
  <td class=xl65 width=319 style='border-left:none;width:239pt'><font
  class="font5">DESCRIPTION</font></td>
  <td class=xl65 width=319 style='border-left:none;width:239pt'><font
  class="font5">SAMPLE SKILLS REQUIRED</font></td>
 </tr>
 <tr height=36 style='mso-height-source:userset;height:27.5pt'>
  <td height=36 class=xl66 width=147 style='height:27.5pt;border-top:none;
  width:110pt'><font class="font6">Database Developer</font></td>
  <td class=xl66 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Create and maintain database-based applications</font></td>
  <td class=xl66 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Programming, database fundamentals, SQL</font></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.5pt'>
  <td height=20 class=xl67 width=147 style='height:15.5pt;border-top:none;
  width:110pt'><font class="font6">Database Designer</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Design and maintain databases</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Systems design, database design, SQL</font></td>
 </tr>
 <tr height=36 style='mso-height-source:userset;height:27.5pt'>
  <td height=36 class=xl66 width=147 style='height:27.5pt;border-top:none;
  width:110pt'><font class="font6">Database Administrator</font></td>
  <td class=xl66 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Manage and maintain DBMS and databases</font></td>
  <td class=xl66 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Database fundamentals, SQL, vendor courses</font></td>
 </tr>
 <tr height=20 style='mso-height-source:userset;height:15.5pt'>
  <td height=20 class=xl67 width=147 style='height:15.5pt;border-top:none;
  width:110pt'><font class="font6">Database Analyst</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Develop databases for decision support reporting</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">SQL, query optimization, data warehouses</font></td>
 </tr>
 <tr height=36 style='mso-height-source:userset;height:27.5pt'>
  <td height=36 class=xl66 width=147 style='height:27.5pt;border-top:none;
  width:110pt'><font class="font6">Database Architect</font></td>
  <td class=xl66 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Design and implementation of database envi- ronments
  (conceptual, logical, and physical)</font></td>
  <td class=xl66 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">DBMS fundamentals, data modeling, SQL, hardware knowledge, etc.</font></td>
 </tr>
 <tr height=52 style='mso-height-source:userset;height:39.5pt'>
  <td height=52 class=xl67 width=147 style='height:39.5pt;border-top:none;
  width:110pt'><font class="font6">Database Consultant</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Help companies leverage database technologies to improve
  business processes and achieve specific goals</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Database fundamentals, data modeling, database design, SQL,
  DBMS, hardware, vendor-specific technologies, etc.</font></td>
 </tr>
 <tr height=36 style='mso-height-source:userset;height:27.5pt'>
  <td height=36 class=xl66 width=147 style='height:27.5pt;border-top:none;
  width:110pt'><font class="font6">Database Security Officer</font></td>
  <td class=xl66 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Implement security policies for data administration</font></td>
  <td class=xl66 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">DBMS fundamentals, database administration, SQL, data security
  technologies, etc.</font></td>
 </tr>
 <tr height=52 style='mso-height-source:userset;height:39.5pt'>
  <td height=52 class=xl67 width=147 style='height:39.5pt;border-top:none;
  width:110pt'><font class="font6">Cloud Computing Data Architect</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Design and implement the infrastructure for next-generation
  cloud database systems</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Internet technologies, cloud storage technologies, data
  security, performance tuning, large databases, etc.</font></td>
 </tr>
 <tr height=52 style='mso-height-source:userset;height:39.5pt'>
  <td height=52 class=xl67 width=147 style='height:39.5pt;border-top:none;
  width:110pt'><font class="font6">Data Scientist</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Analyze large amounts of varied data to generate insights,
  relationships, and predictable behaviors</font></td>
  <td class=xl67 width=319 style='border-top:none;border-left:none;width:239pt'><font
  class="font6">Data analysis, statistics, advanced mathematics, SQL,
  programming, data mining, machine learning, data visualization</font></td>
 </tr>
 <![if supportMisalignedColumns]>
 <tr height=0 style='display:none'>
  <td width=147 style='width:110pt'></td>
  <td width=319 style='width:239pt'></td>
  <td width=319 style='width:239pt'></td>
 </tr>
 <![endif]>
</table>

</div>


<!----------------------------->
<!--END OF OUTPUT FROM EXCEL PUBLISH AS WEB PAGE WIZARD-->
<!----------------------------->
</body>

</html>




