# Summary

* [Introduction](README.md)
* [Chapter 1 Database Systems](lecture1slide.md)
* [Lecture 1](lecture1.md)
* [Lecture 2](lecture2.md)
* [Chapter 2 Data Models](lecture2slide.md)
* [Lecture 3](lecture3.md)
* [Lecture 4](lecture4.md)
* [Lecture 5](lecture5.md)
* [Chapter 3 The Relational Database Model](chapter3.md)



















