<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_003{font-family:Times,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Times,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Arial,serif;font-size:14.0px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_004{font-family:Arial,serif;font-size:14.0px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_005{font-family:Times,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:Times,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:"Calibri",serif;font-size:9.1px;color:rgb(38,37,38);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:"Calibri",serif;font-size:9.1px;color:rgb(38,37,38);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Times,serif;font-size:40.1px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Times,serif;font-size:40.1px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Arial,serif;font-size:28.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Arial,serif;font-size:14.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Times,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:Times,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Arial,serif;font-size:20.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_014{font-family:Arial,serif;font-size:20.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_015{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_016{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_017{font-family:Arial,serif;font-size:24.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Arial,serif;font-size:24.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_018{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_019{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_019{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_020{font-family:Arial,serif;font-size:22.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Arial,serif;font-size:22.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Times,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Times,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:Times,serif;font-size:29.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Times,serif;font-size:29.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Times,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_024{font-family:Times,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_025{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_025{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_026{font-family:Times,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_026{font-family:Times,serif;font-size:36.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_027{font-family:Times,serif;font-size:22.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Times,serif;font-size:22.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_029{font-family:Times,serif;font-size:18.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_030{font-family:Times,serif;font-size:32.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Times,serif;font-size:32.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Times,serif;font-size:36.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Times,serif;font-size:36.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Times,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_032{font-family:Times,serif;font-size:19.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_033{font-family:Times,serif;font-size:29.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Times,serif;font-size:29.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_034{font-family:Times,serif;font-size:21.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_034{font-family:Times,serif;font-size:21.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_035{font-family:Times,serif;font-size:35.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_035{font-family:Times,serif;font-size:35.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture1/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:13.20px;top:8.72px" class="cls_003"><span class="cls_003">11e</span></div>
<div style="position:absolute;left:271.62px;top:11.92px" class="cls_002"><span class="cls_002">Database Systems</span></div>
<div style="position:absolute;left:160.13px;top:35.92px" class="cls_002"><span class="cls_002">Design, Implementation, and Management</span></div>
<div style="position:absolute;left:502.45px;top:279.52px" class="cls_004"><span class="cls_004">Coronel | Morris</span></div>
<div style="position:absolute;left:297.94px;top:374.48px" class="cls_005"><span class="cls_005">Chapter 1</span></div>
<div style="position:absolute;left:231.19px;top:431.60px" class="cls_005"><span class="cls_005">Database Systems</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:197.25px;top:52.88px" class="cls_007"><span class="cls_007">Learning Objectives</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> In this chapter, you will learn:</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> The difference between data and information</span></div>
<div style="position:absolute;left:75.55px;top:211.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> What a database is, the various types of databases, and</span></div>
<div style="position:absolute;left:94.95px;top:242.56px" class="cls_011"><span class="cls_011">why they are valuable assets for decision making</span></div>
<div style="position:absolute;left:75.55px;top:282.64px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> The importance of database design</span></div>
<div style="position:absolute;left:75.55px;top:323.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> How modern databases evolved from file systems</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:704.95px;top:519.52px" class="cls_012"><span class="cls_012">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:197.25px;top:52.88px" class="cls_007"><span class="cls_007">Learning Objectives</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> In this chapter, you will learn:</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> About flaws in file system data management</span></div>
<div style="position:absolute;left:75.55px;top:211.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> The main components of the database system</span></div>
<div style="position:absolute;left:75.55px;top:251.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> The main functions of a database management system</span></div>
<div style="position:absolute;left:94.95px;top:282.64px" class="cls_011"><span class="cls_011">(DBMS)</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:704.95px;top:519.52px" class="cls_012"><span class="cls_012">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:193.81px;top:107.12px" class="cls_005"><span class="cls_005">Data vs. Information</span></div>
<div style="position:absolute;left:171.36px;top:182.36px" class="cls_013"><span class="cls_013">Data</span></div>
<div style="position:absolute;left:482.99px;top:182.36px" class="cls_013"><span class="cls_013">Information</span></div>
<div style="position:absolute;left:45.80px;top:215.92px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Raw facts</span></div>
<div style="position:absolute;left:387.30px;top:215.92px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Produced by processing data</span></div>
<div style="position:absolute;left:69.55px;top:248.80px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Raw data - Not yet been</span></div>
<div style="position:absolute;left:387.30px;top:248.80px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Reveals the meaning of data</span></div>
<div style="position:absolute;left:88.95px;top:272.80px" class="cls_015"><span class="cls_015">processed to reveal the</span></div>
<div style="position:absolute;left:387.30px;top:281.92px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Enables </span><span class="cls_016">knowledge </span><span class="cls_015">creation</span></div>
<div style="position:absolute;left:88.95px;top:296.80px" class="cls_015"><span class="cls_015">meaning</span></div>
<div style="position:absolute;left:387.30px;top:314.80px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Should be accurate, relevant, and</span></div>
<div style="position:absolute;left:45.80px;top:329.92px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Building blocks of information</span></div>
<div style="position:absolute;left:407.45px;top:338.80px" class="cls_015"><span class="cls_015">timely to enable good decision</span></div>
<div style="position:absolute;left:45.80px;top:362.80px" class="cls_014"><span class="cls_014">§</span><span class="cls_016"> Data management</span></div>
<div style="position:absolute;left:407.45px;top:362.80px" class="cls_015"><span class="cls_015">making</span></div>
<div style="position:absolute;left:69.55px;top:395.92px" class="cls_014"><span class="cls_014">§</span><span class="cls_015"> Generation, storage, and</span></div>
<div style="position:absolute;left:88.95px;top:419.92px" class="cls_015"><span class="cls_015">retrieval of data</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:704.95px;top:519.52px" class="cls_012"><span class="cls_012">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:286.69px;top:52.88px" class="cls_007"><span class="cls_007">Database</span></div>
<div style="position:absolute;left:57.85px;top:119.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Shared, integrated computer structure that stores a</span></div>
<div style="position:absolute;left:78.00px;top:147.52px" class="cls_011"><span class="cls_011">collection of:</span></div>
<div style="position:absolute;left:81.60px;top:184.56px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> End-user data - Raw facts of interest to end user</span></div>
<div style="position:absolute;left:81.60px;top:219.60px" class="cls_017"><span class="cls_017">§</span><span class="cls_019"> Metadata</span><span class="cls_018">: Data about data, which the end-user data are</span></div>
<div style="position:absolute;left:101.05px;top:245.52px" class="cls_018"><span class="cls_018">integrated and managed</span></div>
<div style="position:absolute;left:104.65px;top:281.60px" class="cls_020"><span class="cls_020">§</span><span class="cls_021"> Describe data characteristics and relationships</span></div>
<div style="position:absolute;left:57.85px;top:313.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_022"> Database management system (DBMS)</span></div>
<div style="position:absolute;left:81.60px;top:350.64px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> Collection of programs</span></div>
<div style="position:absolute;left:81.60px;top:385.68px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> Manages the database structure</span></div>
<div style="position:absolute;left:81.60px;top:420.48px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> Controls access to data stored in the database</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:704.95px;top:519.52px" class="cls_012"><span class="cls_012">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:209.44px;top:52.88px" class="cls_007"><span class="cls_007">Role of the DBMS</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Intermediary between the user and the database</span></div>
<div style="position:absolute;left:51.80px;top:171.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Enables data to be shared</span></div>
<div style="position:absolute;left:51.80px;top:213.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Presents the end user with an integrated view of the</span></div>
<div style="position:absolute;left:71.95px;top:247.52px" class="cls_009"><span class="cls_009">data</span></div>
<div style="position:absolute;left:51.80px;top:289.52px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Receives and translates application requests into</span></div>
<div style="position:absolute;left:71.95px;top:323.60px" class="cls_009"><span class="cls_009">operations required to fulfill the requests</span></div>
<div style="position:absolute;left:51.80px;top:366.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Hides database’s internal complexity from the</span></div>
<div style="position:absolute;left:71.95px;top:399.68px" class="cls_009"><span class="cls_009">application programs and users</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:704.95px;top:519.52px" class="cls_012"><span class="cls_012">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:77.76px;top:42.28px" class="cls_023"><span class="cls_023">Figure 1.2 - The DBMS Manages the Interaction</span></div>
<div style="position:absolute;left:129.75px;top:77.08px" class="cls_023"><span class="cls_023">between the End User and the Database</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:704.95px;top:519.52px" class="cls_012"><span class="cls_012">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:152.75px;top:52.88px" class="cls_007"><span class="cls_007">Advantages of the DBMS</span></div>
<div style="position:absolute;left:43.20px;top:125.68px" class="cls_024"><span class="cls_024">•</span><span class="cls_011"> Better data integration and less data inconsistency</span></div>
<div style="position:absolute;left:74.70px;top:162.48px" class="cls_017"><span class="cls_017">-</span><span class="cls_019"> Data inconsistency</span><span class="cls_018">: Different versions of the same data</span></div>
<div style="position:absolute;left:101.70px;top:188.64px" class="cls_018"><span class="cls_018">appear in different places</span></div>
<div style="position:absolute;left:43.20px;top:223.60px" class="cls_024"><span class="cls_024">•</span><span class="cls_011"> Increased end-user productivity</span></div>
<div style="position:absolute;left:43.20px;top:260.56px" class="cls_024"><span class="cls_024">•</span><span class="cls_011"> Improved:</span></div>
<div style="position:absolute;left:75.60px;top:297.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Data sharing</span></div>
<div style="position:absolute;left:75.60px;top:334.48px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Data security</span></div>
<div style="position:absolute;left:75.60px;top:371.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Data access</span></div>
<div style="position:absolute;left:75.60px;top:408.64px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Decision making</span></div>
<div style="position:absolute;left:98.65px;top:446.64px" class="cls_017"><span class="cls_017">§</span><span class="cls_019"> Data quality</span><span class="cls_018">: Promoting accuracy, validity, and</span></div>
<div style="position:absolute;left:115.95px;top:472.56px" class="cls_018"><span class="cls_018">timeliness of data</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:704.95px;top:519.52px" class="cls_012"><span class="cls_012">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.71px;top:52.88px" class="cls_007"><span class="cls_007">Types of Databases</span></div>
<div style="position:absolute;left:57.80px;top:122.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Single-user database</span><span class="cls_009">: Supports one user at a time</span></div>
<div style="position:absolute;left:81.55px;top:165.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_022"> Desktop database</span><span class="cls_011">: Runs on PC</span></div>
<div style="position:absolute;left:57.80px;top:205.52px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Multiuser database</span><span class="cls_009">: Supports multiple users at the</span></div>
<div style="position:absolute;left:77.95px;top:238.64px" class="cls_009"><span class="cls_009">same time</span></div>
<div style="position:absolute;left:81.55px;top:281.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_022"> Workgroup databases</span><span class="cls_011">: Supports a small number of</span></div>
<div style="position:absolute;left:100.95px;top:312.64px" class="cls_011"><span class="cls_011">users or a specific department</span></div>
<div style="position:absolute;left:81.55px;top:353.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_022"> Enterprise database</span><span class="cls_011">: Supports many users across</span></div>
<div style="position:absolute;left:100.95px;top:384.64px" class="cls_011"><span class="cls_011">many departments</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:704.95px;top:519.52px" class="cls_012"><span class="cls_012">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.71px;top:52.88px" class="cls_007"><span class="cls_007">Types of Databases</span></div>
<div style="position:absolute;left:57.85px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Centralized database</span><span class="cls_009">: Data is located at a single</span></div>
<div style="position:absolute;left:78.00px;top:162.56px" class="cls_009"><span class="cls_009">site</span></div>
<div style="position:absolute;left:57.85px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Distributed database</span><span class="cls_009">: Data is distributed across</span></div>
<div style="position:absolute;left:78.00px;top:238.64px" class="cls_009"><span class="cls_009">different sites</span></div>
<div style="position:absolute;left:57.85px;top:280.64px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Cloud database</span><span class="cls_009">: Created and maintained using</span></div>
<div style="position:absolute;left:78.00px;top:314.48px" class="cls_009"><span class="cls_009">cloud data services that provide defined</span></div>
<div style="position:absolute;left:78.00px;top:348.56px" class="cls_009"><span class="cls_009">performance measures for the database</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.71px;top:52.88px" class="cls_007"><span class="cls_007">Types of Databases</span></div>
<div style="position:absolute;left:57.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> General-purpose databases</span><span class="cls_009">: Contains a wide</span></div>
<div style="position:absolute;left:77.95px;top:162.56px" class="cls_009"><span class="cls_009">variety of data used in multiple disciplines</span></div>
<div style="position:absolute;left:57.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Discipline-specific databases</span><span class="cls_009">: Contains data</span></div>
<div style="position:absolute;left:77.95px;top:238.64px" class="cls_009"><span class="cls_009">focused on specific subject areas</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:698.24px;top:519.52px" class="cls_012"><span class="cls_012">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.71px;top:52.88px" class="cls_007"><span class="cls_007">Types of Databases</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Operational database</span><span class="cls_009">: Designed to support a</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">company’s day-to-day operations</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Analytical database</span><span class="cls_009">: Stores historical data and</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_009"><span class="cls_009">business metrics used exclusively for tactical or</span></div>
<div style="position:absolute;left:71.95px;top:271.52px" class="cls_009"><span class="cls_009">strategic decision making</span></div>
<div style="position:absolute;left:75.55px;top:314.56px" class="cls_010"><span class="cls_010">§</span><span class="cls_022"> Data warehouse</span><span class="cls_011">: Stores data in a format optimized for</span></div>
<div style="position:absolute;left:94.95px;top:345.52px" class="cls_011"><span class="cls_011">decision support</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.71px;top:52.88px" class="cls_007"><span class="cls_007">Types of Databases</span></div>
<div style="position:absolute;left:75.55px;top:128.56px" class="cls_010"><span class="cls_010">§</span><span class="cls_022"> Online analytical processing (OLAP)</span></div>
<div style="position:absolute;left:98.60px;top:168.48px" class="cls_017"><span class="cls_017">§</span><span class="cls_018"> Enable retrieving, processing, and modeling data from the</span></div>
<div style="position:absolute;left:115.85px;top:197.52px" class="cls_018"><span class="cls_018">data warehouse</span></div>
<div style="position:absolute;left:75.55px;top:235.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_022"> Business intelligence</span><span class="cls_011">: Captures and processes business</span></div>
<div style="position:absolute;left:94.95px;top:266.56px" class="cls_011"><span class="cls_011">data to generate information that support decision</span></div>
<div style="position:absolute;left:94.95px;top:297.52px" class="cls_011"><span class="cls_011">making</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.71px;top:52.88px" class="cls_007"><span class="cls_007">Types of Databases</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Unstructured data</span><span class="cls_009">: It exists in their original state</span></div>
<div style="position:absolute;left:51.80px;top:171.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Structured data</span><span class="cls_009">: It results from formatting</span></div>
<div style="position:absolute;left:75.55px;top:213.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Structure is applied based on type of processing to be</span></div>
<div style="position:absolute;left:94.95px;top:245.68px" class="cls_011"><span class="cls_011">performed</span></div>
<div style="position:absolute;left:51.80px;top:285.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Semistructured data</span><span class="cls_009">: Processed to some extent</span></div>
<div style="position:absolute;left:51.80px;top:327.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Extensible Markup Language (XML)</span></div>
<div style="position:absolute;left:75.55px;top:370.48px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Represents data elements in textual format</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:225.00px;top:52.88px" class="cls_007"><span class="cls_007">Database Design</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Focuses on the design of the database structure that</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">will be used to store and manage end-user data</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Well-designed database</span></div>
<div style="position:absolute;left:75.55px;top:247.60px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Facilitates data management</span></div>
<div style="position:absolute;left:75.55px;top:287.68px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Generates accurate and valuable information</span></div>
<div style="position:absolute;left:51.80px;top:327.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Poorly designed database causes difficult-to-trace</span></div>
<div style="position:absolute;left:71.95px;top:361.52px" class="cls_009"><span class="cls_009">errors</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:58.50px;top:55.44px" class="cls_026"><span class="cls_026">Evolution of File System Data Processing</span></div>
<div style="position:absolute;left:264.12px;top:140.00px" class="cls_027"><span class="cls_027">Manual File Systems</span></div>
<div style="position:absolute;left:121.31px;top:187.68px" class="cls_028"><span class="cls_028">Accomplished through a system of file folders and filing cabinets</span></div>
<div style="position:absolute;left:235.41px;top:275.60px" class="cls_027"><span class="cls_027">Computerized File Systems</span></div>
<div style="position:absolute;left:34.67px;top:314.16px" class="cls_029"><span class="cls_029">Data processing (DP) specialist</span><span class="cls_028">: Created a computer-based system that would track data</span></div>
<div style="position:absolute;left:253.29px;top:332.16px" class="cls_028"><span class="cls_028">and produce required reports</span></div>
<div style="position:absolute;left:103.78px;top:410.96px" class="cls_027"><span class="cls_027">File System Redux: Modern End-User Productivity Tools</span></div>
<div style="position:absolute;left:157.06px;top:456.72px" class="cls_028"><span class="cls_028">Includes spreadsheet programs such as Microsoft Excel</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:136.77px;top:57.76px" class="cls_030"><span class="cls_030">Table 1.2 - Basic File Terminology</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:145.83px;top:57.76px" class="cls_030"><span class="cls_030">Figure1.6 - A Simple File System</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:44.50px;top:55.44px" class="cls_031"><span class="cls_031">Problems with File System Data Processing</span></div>
<div style="position:absolute;left:98.78px;top:143.00px" class="cls_032"><span class="cls_032">Lengthy development times</span></div>
<div style="position:absolute;left:98.78px;top:210.92px" class="cls_032"><span class="cls_032">Difficulty of getting quick answers</span></div>
<div style="position:absolute;left:90.83px;top:279.08px" class="cls_032"><span class="cls_032">Complex system administration</span></div>
<div style="position:absolute;left:98.78px;top:346.52px" class="cls_032"><span class="cls_032">Lack of security and limited data sharing</span></div>
<div style="position:absolute;left:98.78px;top:414.44px" class="cls_032"><span class="cls_032">Extensive programming</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:100.63px;top:52.88px" class="cls_007"><span class="cls_007">Structural and Data Dependence</span></div>
<div style="position:absolute;left:51.85px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Structural dependence</span><span class="cls_009">: Access to a file is dependent</span></div>
<div style="position:absolute;left:72.00px;top:162.56px" class="cls_009"><span class="cls_009">on its own structure</span></div>
<div style="position:absolute;left:75.60px;top:204.64px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> All file system programs are modified to conform to a</span></div>
<div style="position:absolute;left:95.05px;top:236.56px" class="cls_011"><span class="cls_011">new file structure</span></div>
<div style="position:absolute;left:51.85px;top:276.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Structural independence</span><span class="cls_009">: File structure is changed</span></div>
<div style="position:absolute;left:72.00px;top:309.68px" class="cls_009"><span class="cls_009">without affecting the application’s ability to access</span></div>
<div style="position:absolute;left:72.00px;top:343.52px" class="cls_009"><span class="cls_009">the data</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:100.63px;top:52.88px" class="cls_007"><span class="cls_007">Structural and Data Dependence</span></div>
<div style="position:absolute;left:57.80px;top:122.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Data dependence</span></div>
<div style="position:absolute;left:81.55px;top:165.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Data access changes when data storage characteristics</span></div>
<div style="position:absolute;left:100.95px;top:196.48px" class="cls_011"><span class="cls_011">change</span></div>
<div style="position:absolute;left:57.80px;top:236.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Data independence</span></div>
<div style="position:absolute;left:81.55px;top:279.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Data storage characteristics is changed without</span></div>
<div style="position:absolute;left:100.95px;top:310.48px" class="cls_011"><span class="cls_011">affecting the program’s ability to access the data</span></div>
<div style="position:absolute;left:57.80px;top:350.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Practical significance of data dependence is</span></div>
<div style="position:absolute;left:77.95px;top:384.56px" class="cls_009"><span class="cls_009">difference between logical and physical format</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:217.31px;top:52.88px" class="cls_005"><span class="cls_005">Data Redundancy</span></div>
<div style="position:absolute;left:51.85px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Unnecessarily storing same data at different places</span></div>
<div style="position:absolute;left:51.85px;top:171.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Islands of information</span><span class="cls_009">: Scattered data locations</span></div>
<div style="position:absolute;left:75.60px;top:213.52px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Increases the probability of having different versions of</span></div>
<div style="position:absolute;left:95.05px;top:245.68px" class="cls_011"><span class="cls_011">the same data</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:112.25px;top:52.88px" class="cls_005"><span class="cls_005">Data Redundancy Implications</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Poor data security</span></div>
<div style="position:absolute;left:51.80px;top:171.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Data inconsistency</span></div>
<div style="position:absolute;left:51.80px;top:213.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Increased likelihood of data-entry errors when</span></div>
<div style="position:absolute;left:71.95px;top:247.52px" class="cls_009"><span class="cls_009">complex entries are made in different files</span></div>
<div style="position:absolute;left:51.80px;top:289.52px" class="cls_008"><span class="cls_008">§</span><span class="cls_025"> Data anomaly</span><span class="cls_009">: Develops when not all of the required</span></div>
<div style="position:absolute;left:71.95px;top:323.60px" class="cls_009"><span class="cls_009">changes in the redundant data are made successfully</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:169.75px;top:52.88px" class="cls_005"><span class="cls_005">Types of Data Anomaly</span></div>
<div style="position:absolute;left:104.08px;top:145.96px" class="cls_033"><span class="cls_033">Update Anomalies</span></div>
<div style="position:absolute;left:104.08px;top:249.64px" class="cls_033"><span class="cls_033">Insertion Anomalies</span></div>
<div style="position:absolute;left:104.08px;top:353.32px" class="cls_033"><span class="cls_033">Deletion Anomalies</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:58.50px;top:55.44px" class="cls_031"><span class="cls_031">Lack of Design and Data-Modeling Skills</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Evident despite the availability of multiple personal</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_009"><span class="cls_009">productivity tools being available</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Data-modeling skills is vital in the data design</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_009"><span class="cls_009">process</span></div>
<div style="position:absolute;left:51.80px;top:280.64px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Good data modeling facilitates communication</span></div>
<div style="position:absolute;left:71.95px;top:314.48px" class="cls_009"><span class="cls_009">between the designer, user, and the developer</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:214.94px;top:52.88px" class="cls_007"><span class="cls_007">Database Systems</span></div>
<div style="position:absolute;left:51.85px;top:128.48px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Logically related data stored in a single logical data</span></div>
<div style="position:absolute;left:72.00px;top:162.56px" class="cls_009"><span class="cls_009">repository</span></div>
<div style="position:absolute;left:75.60px;top:204.64px" class="cls_010"><span class="cls_010">§</span><span class="cls_011"> Physically distributed among multiple storage facilities</span></div>
<div style="position:absolute;left:43.20px;top:245.68px" class="cls_024"><span class="cls_024">•</span><span class="cls_011"> DBMS eliminates most of file system’s problems</span></div>
<div style="position:absolute;left:43.20px;top:285.68px" class="cls_008"><span class="cls_008">§</span><span class="cls_009"> Current generation DBMS software:</span></div>
<div style="position:absolute;left:74.70px;top:327.60px" class="cls_017"><span class="cls_017">-</span><span class="cls_018"> Stores data structures, relationships between structures, and</span></div>
<div style="position:absolute;left:101.70px;top:356.64px" class="cls_018"><span class="cls_018">access paths</span></div>
<div style="position:absolute;left:74.70px;top:394.56px" class="cls_017"><span class="cls_017">-</span><span class="cls_018"> Defines, stores, and manages all access paths and</span></div>
<div style="position:absolute;left:101.70px;top:423.60px" class="cls_018"><span class="cls_018">components</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:75.06px;top:38.56px" class="cls_030"><span class="cls_030">Figure 1.8 - Contrasting Database and   File</span></div>
<div style="position:absolute;left:306.63px;top:76.48px" class="cls_030"><span class="cls_030">Systems</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:54.98px;top:57.76px" class="cls_030"><span class="cls_030">Figure 1.9 - The Database System Environment</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:219.38px;top:52.88px" class="cls_005"><span class="cls_005">DBMS Functions</span></div>
<div style="position:absolute;left:50.73px;top:136.68px" class="cls_034"><span class="cls_034">Data dictionary management</span></div>
<div style="position:absolute;left:62.19px;top:181.68px" class="cls_028"><span class="cls_028">•</span><span class="cls_029"> Data dictionary</span><span class="cls_028">: Stores definitions of the data elements and their relationships</span></div>
<div style="position:absolute;left:50.73px;top:236.76px" class="cls_034"><span class="cls_034">Data storage management</span></div>
<div style="position:absolute;left:62.19px;top:277.20px" class="cls_028"><span class="cls_028">•</span><span class="cls_029"> Performance tuning</span><span class="cls_028">: Ensures efficient performance of the database in terms of</span></div>
<div style="position:absolute;left:80.19px;top:297.12px" class="cls_028"><span class="cls_028">storage and access speed</span></div>
<div style="position:absolute;left:50.73px;top:334.20px" class="cls_034"><span class="cls_034">Data transformation and presentation</span></div>
<div style="position:absolute;left:62.19px;top:383.04px" class="cls_028"><span class="cls_028">• Transforms entered data to conform to required data structures</span></div>
<div style="position:absolute;left:50.73px;top:422.76px" class="cls_034"><span class="cls_034">Security management</span></div>
<div style="position:absolute;left:62.19px;top:463.20px" class="cls_028"><span class="cls_028">• Enforces user security and data privacy</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:219.38px;top:52.88px" class="cls_005"><span class="cls_005">DBMS Functions</span></div>
<div style="position:absolute;left:54.09px;top:141.44px" class="cls_009"><span class="cls_009">Multiuser access control</span></div>
<div style="position:absolute;left:62.19px;top:200.08px" class="cls_015"><span class="cls_015">• Sophisticated algorithms ensure that multiple users can access the</span></div>
<div style="position:absolute;left:80.19px;top:222.16px" class="cls_015"><span class="cls_015">database concurrently without compromising its integrity</span></div>
<div style="position:absolute;left:54.09px;top:273.44px" class="cls_009"><span class="cls_009">Backup and recovery management</span></div>
<div style="position:absolute;left:62.19px;top:338.08px" class="cls_015"><span class="cls_015">• Enables recovery of the database after a failure</span></div>
<div style="position:absolute;left:54.09px;top:388.16px" class="cls_009"><span class="cls_009">Data integrity management</span></div>
<div style="position:absolute;left:62.19px;top:452.08px" class="cls_015"><span class="cls_015">• Minimizes redundancy and maximizes consistency</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:219.38px;top:52.88px" class="cls_005"><span class="cls_005">DBMS Functions</span></div>
<div style="position:absolute;left:52.80px;top:138.96px" class="cls_018"><span class="cls_018">Database access languages and application programming</span></div>
<div style="position:absolute;left:52.80px;top:165.12px" class="cls_018"><span class="cls_018">interfaces</span></div>
<div style="position:absolute;left:62.19px;top:209.20px" class="cls_015"><span class="cls_015">•</span><span class="cls_016"> Query language</span><span class="cls_015">: Lets the user specify what must be done without having</span></div>
<div style="position:absolute;left:80.19px;top:231.28px" class="cls_015"><span class="cls_015">to specify how</span></div>
<div style="position:absolute;left:62.19px;top:257.20px" class="cls_015"><span class="cls_015">•</span><span class="cls_016"> Structured Query Language (SQL)</span><span class="cls_015">: De facto query language and data</span></div>
<div style="position:absolute;left:80.19px;top:279.28px" class="cls_015"><span class="cls_015">access standard supported by the majority of DBMS vendors</span></div>
<div style="position:absolute;left:52.80px;top:337.92px" class="cls_018"><span class="cls_018">Database communication interfaces</span></div>
<div style="position:absolute;left:62.19px;top:407.12px" class="cls_021"><span class="cls_021">• Accept end-user requests via multiple, different network</span></div>
<div style="position:absolute;left:80.19px;top:430.16px" class="cls_021"><span class="cls_021">environments</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:71.56px;top:52.88px" class="cls_007"><span class="cls_007">Disadvantages of Database Systems</span></div>
<div style="position:absolute;left:92.90px;top:147.04px" class="cls_015"><span class="cls_015">Increased costs</span></div>
<div style="position:absolute;left:92.90px;top:218.32px" class="cls_015"><span class="cls_015">Management complexity</span></div>
<div style="position:absolute;left:92.90px;top:289.84px" class="cls_015"><span class="cls_015">Maintaining currency</span></div>
<div style="position:absolute;left:92.90px;top:361.36px" class="cls_015"><span class="cls_015">Vendor dependence</span></div>
<div style="position:absolute;left:92.90px;top:432.64px" class="cls_015"><span class="cls_015">Frequent upgrade/replacement cycles</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:697.20px;top:519.52px" class="cls_012"><span class="cls_012">32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture1/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:680.83px;top:8.56px" class="cls_012"><span class="cls_012">33</span></div>
<div style="position:absolute;left:69.22px;top:58.60px" class="cls_035"><span class="cls_035">Table 1.3 - Database Career Opportunities</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_006"><span class="cls_006">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
</div>

</body>
</html>
