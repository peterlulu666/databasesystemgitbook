## Quiz      

1. The Object-Oriented Model 
    - In the **object-oriented data model (OODM)**, both data and its relationships are contained in a single structure known as an **object** 
        - In turn, the **OODM** is the **basis** for the **object-oriented database management system (OODBMs)** 
    - **The objects** include **data**, various types of **relationships**, and **operational procedures** 
        - a basic building block for autonomous structures 
        - An object is an abstraction of a real-world entity 
    - **Attributes** describe the properties of an object 
    - A **class** is a collection of similar objects with shared structure (attributes) and behavior (methods) 
    - **Classes** are organized in a class hierarchy. The class hierarchy resembles an **upside-down tree** in which each class has **only one parent** 
    - **inheritance** is the ability of an object within the class hierarchy to inherit the attri- butes and methods of the classes above it 
    - **UML** is a language based on OO concepts that describes a set of diagrams and symbols you can use to graphically model a system 

2. The Object/Relational and XML 
    - extended relational data model (ERDM) 
        - The **ERDM** adds many of the OO model’s features within the inherently simpler relational database structure. The ERDM gave birth to a new generation of relational databases that **support OO features such as objects (encapsulated data and methods), extensible data types based on classes, and inheritance** 
        - the **ERDM** is often described as an **object/relational database management system (O/R DBMS)** 
    - extensible Markup Language (xML) emerged
        - as the de facto standard for the efficient and effective exchange of structured, semistructured, and unstructured data 

3. The Emerging Data Models: Big Data and NoSQL 
    - Big Data refers to a movement to find new and better ways to manage **large amounts of web and sensor-generated data** and derive business insight from it, while simultaneously providing **high performance and scalability at a reasonable cost** 
    - characteristics of Big Data databases 
        - **volume** 
            - Volume refers to the amounts of data being stored 
        - **velocity** 
            - Velocity refers not only to the speed with which data grows but also to the need to process this data quickly in order to generate information and insight 
        - **variety** 
            - Variety refers to the fact that the data being collected comes in multiple different data format 
    - The Big Data challenges 
        - It is **not always possible to fit** unstructured, social media and sensor-generated data into the conventional relational structure of rows and columns 
        - The type of high-volume implementations required in the RDBMS environ- ment for the Big Data problem comes with a **hefty price** tag for expanding hardware, storage, and software licenses 
        - OLAP tools mining for usable data in the vast amounts of **unstructured data** collected from web sources requires a different approach 
    - Big Data technologies 
        - Hadoop 
        - Hadoop Distributed File system (HDFs) 
        - MapReduce 
        - NoSQL databases 
            - They are not based on the relational model and SQL; hence the name NoSQL 
            - They support highly distributed database architectures 
            - They provide high scalability, high availability, and fault tolerance 
            - They support very large amounts of sparse data (data with a large number of attri- butes but where the actual number of data instances is low) 
            - They are geared toward performance rather than transaction consistency 
            - the NoSQL model is a broad umbrella for a variety of approaches to data storage and manipulation 

































    


