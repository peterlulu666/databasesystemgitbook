<html>
<head><meta http-equiv=Content-Type content="text/html; charset=UTF-8">
<style type="text/css">
<!--
span.cls_003{font-family:Times,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_003{font-family:Times,serif;font-size:16.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_002{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_002{font-family:Arial,serif;font-size:20.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_004{font-family:Times,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_004{font-family:Times,serif;font-size:40.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_005{font-family:"Calibri",serif;font-size:9.1px;color:rgb(38,37,38);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_005{font-family:"Calibri",serif;font-size:9.1px;color:rgb(38,37,38);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_006{font-family:Times,serif;font-size:40.1px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_006{font-family:Times,serif;font-size:40.1px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_007{font-family:Arial,serif;font-size:28.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_007{font-family:Arial,serif;font-size:28.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_008{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_008{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_009{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_009{font-family:Arial,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_010{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_010{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_011{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_011{font-family:Arial,serif;font-size:12.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_012{font-family:Times,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_012{font-family:Times,serif;font-size:26.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_013{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_013{font-family:Times,serif;font-size:26.0px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_014{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_014{font-family:Times,serif;font-size:28.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_015{font-family:Times,serif;font-size:24.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_015{font-family:Times,serif;font-size:24.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_016{font-family:Times,serif;font-size:29.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_016{font-family:Times,serif;font-size:29.0px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_017{font-family:Times,serif;font-size:36.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_017{font-family:Times,serif;font-size:36.0px;color:rgb(66,67,86);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_018{font-family:Times,serif;font-size:19.0px;color:rgb(63,63,63);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_018{font-family:Times,serif;font-size:19.0px;color:rgb(63,63,63);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_019{font-family:Arial,serif;font-size:22.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_019{font-family:Arial,serif;font-size:22.0px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_020{font-family:Times,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_020{font-family:Times,serif;font-size:22.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_021{font-family:Arial,serif;font-size:20.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_021{font-family:Arial,serif;font-size:20.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_022{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_022{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_023{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_023{font-family:Times,serif;font-size:20.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_024{font-family:Times,serif;font-size:30.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_024{font-family:Times,serif;font-size:30.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_025{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_025{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_026{font-family:Times,serif;font-size:34.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_026{font-family:Times,serif;font-size:34.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_027{font-family:Times,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_027{font-family:Times,serif;font-size:27.1px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_028{font-family:Arial,serif;font-size:24.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_028{font-family:Arial,serif;font-size:24.1px;color:rgb(0,111,192);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_029{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_029{font-family:Times,serif;font-size:24.1px;color:rgb(0,0,0);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_030{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_030{font-family:Arial,serif;font-size:10.0px;color:rgb(0,0,0);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_031{font-family:Times,serif;font-size:31.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_031{font-family:Times,serif;font-size:31.1px;color:rgb(255,254,255);font-weight:normal;font-style:normal;text-decoration: none}
span.cls_032{font-family:Times,serif;font-size:28.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
div.cls_032{font-family:Times,serif;font-size:28.1px;color:rgb(255,254,255);font-weight:bold;font-style:normal;text-decoration: none}
span.cls_033{font-family:Times,serif;font-size:20.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
div.cls_033{font-family:Times,serif;font-size:20.1px;color:rgb(255,0,0);font-weight:normal;font-style:normal;text-decoration: none}
-->
</style>
<script type="text/javascript" src="lecture2/wz_jsgraphics.js"></script>
</head>
<body>
<div style="position:absolute;left:50%;margin-left:-360px;top:0px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background01.jpg" width=720 height=540></div>
<div style="position:absolute;left:13.20px;top:8.72px" class="cls_003"><span class="cls_003">11e</span></div>
<div style="position:absolute;left:271.62px;top:11.92px" class="cls_002"><span class="cls_002">Database Systems</span></div>
<div style="position:absolute;left:160.13px;top:35.92px" class="cls_002"><span class="cls_002">Design, Implementation, and Management</span></div>
<div style="position:absolute;left:297.94px;top:374.48px" class="cls_004"><span class="cls_004">Chapter 2</span></div>
<div style="position:absolute;left:273.44px;top:431.60px" class="cls_004"><span class="cls_004">Data Models</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background02.jpg" width=720 height=540></div>
<div style="position:absolute;left:197.25px;top:52.88px" class="cls_006"><span class="cls_006">Learning Objectives</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> In this chapter, you will learn:</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> About data modeling and why data models are</span></div>
<div style="position:absolute;left:94.95px;top:202.48px" class="cls_010"><span class="cls_010">important</span></div>
<div style="position:absolute;left:75.55px;top:242.56px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> About the basic data-modeling building blocks</span></div>
<div style="position:absolute;left:75.55px;top:282.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> What business rules are and how they influence</span></div>
<div style="position:absolute;left:94.95px;top:314.56px" class="cls_010"><span class="cls_010">database design</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:706.08px;top:522.00px" class="cls_011"><span class="cls_011">2</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background03.jpg" width=720 height=540></div>
<div style="position:absolute;left:197.25px;top:52.88px" class="cls_006"><span class="cls_006">Learning Objectives</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> In this chapter, you will learn:</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> How the major data models evolved</span></div>
<div style="position:absolute;left:75.55px;top:211.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> About emerging alternative data models and the need</span></div>
<div style="position:absolute;left:94.95px;top:242.56px" class="cls_010"><span class="cls_010">they fulfill</span></div>
<div style="position:absolute;left:75.55px;top:282.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> How data models can be classified by their level of</span></div>
<div style="position:absolute;left:94.95px;top:314.56px" class="cls_010"><span class="cls_010">abstraction</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:706.08px;top:522.00px" class="cls_011"><span class="cls_011">3</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:1650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background04.jpg" width=720 height=540></div>
<div style="position:absolute;left:97.75px;top:52.88px" class="cls_006"><span class="cls_006">Data Modeling and Data Models</span></div>
<div style="position:absolute;left:43.20px;top:128.56px" class="cls_012"><span class="cls_012">•</span><span class="cls_013"> Data modeling</span><span class="cls_010">: Iterative and progressive process of</span></div>
<div style="position:absolute;left:70.20px;top:159.52px" class="cls_010"><span class="cls_010">creating a specific data model for a determined problem</span></div>
<div style="position:absolute;left:70.20px;top:191.68px" class="cls_010"><span class="cls_010">domain</span></div>
<div style="position:absolute;left:51.85px;top:231.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Data models</span><span class="cls_008">: Simple representations of complex</span></div>
<div style="position:absolute;left:72.00px;top:264.56px" class="cls_008"><span class="cls_008">real-world data structures</span></div>
<div style="position:absolute;left:75.60px;top:307.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Useful for supporting a specific problem domain</span></div>
<div style="position:absolute;left:51.85px;top:347.60px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Model</span><span class="cls_008"> - Abstraction of a real-world object or event</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:706.08px;top:522.00px" class="cls_011"><span class="cls_011">4</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background05.jpg" width=720 height=540></div>
<div style="position:absolute;left:139.38px;top:52.88px" class="cls_006"><span class="cls_006">Importance of Data Models</span></div>
<div style="position:absolute;left:99.00px;top:135.36px" class="cls_015"><span class="cls_015">Are a communication tool</span></div>
<div style="position:absolute;left:99.00px;top:223.20px" class="cls_015"><span class="cls_015">Give an overall view of the database</span></div>
<div style="position:absolute;left:99.00px;top:311.04px" class="cls_015"><span class="cls_015">Organize data for various users</span></div>
<div style="position:absolute;left:99.00px;top:387.12px" class="cls_015"><span class="cls_015">Are an abstraction for the creation of good</span></div>
<div style="position:absolute;left:99.00px;top:411.12px" class="cls_015"><span class="cls_015">database</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:706.08px;top:522.00px" class="cls_011"><span class="cls_011">5</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:2750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background06.jpg" width=720 height=540></div>
<div style="position:absolute;left:80.00px;top:52.88px" class="cls_006"><span class="cls_006">Data Model Basic Building Blocks</span></div>
<div style="position:absolute;left:69.80px;top:122.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Entity</span><span class="cls_008">: Unique and distinct object used to collect</span></div>
<div style="position:absolute;left:89.95px;top:156.56px" class="cls_008"><span class="cls_008">and store data</span></div>
<div style="position:absolute;left:93.55px;top:198.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_013"> Attribute</span><span class="cls_010">: Characteristic of an entity</span></div>
<div style="position:absolute;left:69.80px;top:238.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Relationship</span><span class="cls_008">: Describes an association among</span></div>
<div style="position:absolute;left:89.95px;top:272.48px" class="cls_008"><span class="cls_008">entities</span></div>
<div style="position:absolute;left:93.55px;top:315.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_013"> One-to-many (1:M)</span></div>
<div style="position:absolute;left:93.55px;top:355.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_013"> Many-to-many (M:N or M:M)</span></div>
<div style="position:absolute;left:93.55px;top:395.68px" class="cls_009"><span class="cls_009">§</span><span class="cls_013"> One-to-one (1:1)</span></div>
<div style="position:absolute;left:69.80px;top:435.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Constraint</span><span class="cls_008">: Set of rules to ensure data integrity</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:706.08px;top:522.00px" class="cls_011"><span class="cls_011">6</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background07.jpg" width=720 height=540></div>
<div style="position:absolute;left:238.25px;top:52.88px" class="cls_006"><span class="cls_006">Business Rules</span></div>
<div style="position:absolute;left:106.16px;top:129.60px" class="cls_015"><span class="cls_015">Brief, precise, and unambiguous description of a</span></div>
<div style="position:absolute;left:106.16px;top:153.60px" class="cls_015"><span class="cls_015">policy, procedure, or principle</span></div>
<div style="position:absolute;left:106.16px;top:275.04px" class="cls_015"><span class="cls_015">Enable defining the basic building blocks</span></div>
<div style="position:absolute;left:106.16px;top:396.72px" class="cls_015"><span class="cls_015">Describe main and distinguishing characteristics</span></div>
<div style="position:absolute;left:106.16px;top:420.72px" class="cls_015"><span class="cls_015">of the data</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:706.08px;top:522.00px" class="cls_011"><span class="cls_011">7</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:3850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background08.jpg" width=720 height=540></div>
<div style="position:absolute;left:148.19px;top:52.88px" class="cls_006"><span class="cls_006">Sources of Business Rules</span></div>
<div style="position:absolute;left:93.24px;top:180.28px" class="cls_016"><span class="cls_016">Company</span></div>
<div style="position:absolute;left:501.94px;top:180.28px" class="cls_016"><span class="cls_016">Department</span></div>
<div style="position:absolute;left:277.45px;top:194.68px" class="cls_016"><span class="cls_016">Policy makers</span></div>
<div style="position:absolute;left:94.07px;top:209.08px" class="cls_016"><span class="cls_016">managers</span></div>
<div style="position:absolute;left:514.82px;top:209.08px" class="cls_016"><span class="cls_016">managers</span></div>
<div style="position:absolute;left:428.96px;top:299.56px" class="cls_016"><span class="cls_016">Direct</span></div>
<div style="position:absolute;left:211.11px;top:313.96px" class="cls_016"><span class="cls_016">Written</span></div>
<div style="position:absolute;left:404.79px;top:328.60px" class="cls_016"><span class="cls_016">interviews</span></div>
<div style="position:absolute;left:168.65px;top:343.00px" class="cls_016"><span class="cls_016">documentation</span></div>
<div style="position:absolute;left:381.43px;top:356.68px" class="cls_016"><span class="cls_016">with end users</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:706.08px;top:522.00px" class="cls_011"><span class="cls_011">8</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background09.jpg" width=720 height=540></div>
<div style="position:absolute;left:57.00px;top:33.84px" class="cls_017"><span class="cls_017">Reasons for Identifying and Documenting</span></div>
<div style="position:absolute;left:250.50px;top:76.80px" class="cls_017"><span class="cls_017">Business Rules</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Help standardize company’s view of data</span></div>
<div style="position:absolute;left:51.80px;top:171.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Communications tool between users and designers</span></div>
<div style="position:absolute;left:51.80px;top:213.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Allow designer to:</span></div>
<div style="position:absolute;left:75.55px;top:256.48px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Understand the nature, role, scope of data, and business</span></div>
<div style="position:absolute;left:94.95px;top:287.68px" class="cls_010"><span class="cls_010">processes</span></div>
<div style="position:absolute;left:75.55px;top:327.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Develop appropriate relationship participation rules and</span></div>
<div style="position:absolute;left:94.95px;top:359.68px" class="cls_010"><span class="cls_010">constraints</span></div>
<div style="position:absolute;left:75.55px;top:399.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Create an accurate data model</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:706.08px;top:522.00px" class="cls_011"><span class="cls_011">9</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:4950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background10.jpg" width=720 height=540></div>
<div style="position:absolute;left:84.63px;top:33.84px" class="cls_017"><span class="cls_017">Translating Business Rules into   Data</span></div>
<div style="position:absolute;left:217.50px;top:76.80px" class="cls_017"><span class="cls_017">Model Components</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Nouns translate into entities</span></div>
<div style="position:absolute;left:51.80px;top:171.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Verbs translate into relationships among entities</span></div>
<div style="position:absolute;left:51.80px;top:213.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Relationships are bidirectional</span></div>
<div style="position:absolute;left:51.80px;top:256.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Questions to identify the relationship type</span></div>
<div style="position:absolute;left:75.55px;top:299.68px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> How many instances of B are related to one instance of</span></div>
<div style="position:absolute;left:94.95px;top:330.64px" class="cls_010"><span class="cls_010">A?</span></div>
<div style="position:absolute;left:75.55px;top:370.48px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> How many instances of A are related to one instance of</span></div>
<div style="position:absolute;left:94.95px;top:401.68px" class="cls_010"><span class="cls_010">B?</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">10</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:5500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background11.jpg" width=720 height=540></div>
<div style="position:absolute;left:189.44px;top:52.88px" class="cls_006"><span class="cls_006">Naming Conventions</span></div>
<div style="position:absolute;left:69.80px;top:122.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Entity names - Required to:</span></div>
<div style="position:absolute;left:93.55px;top:165.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Be descriptive of the objects in the business</span></div>
<div style="position:absolute;left:112.95px;top:196.48px" class="cls_010"><span class="cls_010">environment</span></div>
<div style="position:absolute;left:93.55px;top:236.56px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Use terminology that is familiar to the users</span></div>
<div style="position:absolute;left:69.80px;top:276.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Attribute name - Required to be descriptive of the</span></div>
<div style="position:absolute;left:89.95px;top:310.64px" class="cls_008"><span class="cls_008">data represented by the attribute</span></div>
<div style="position:absolute;left:69.80px;top:352.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Proper naming:</span></div>
<div style="position:absolute;left:93.55px;top:395.68px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Facilitates communication between parties</span></div>
<div style="position:absolute;left:93.55px;top:435.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Promotes self-documentation</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:700.34px;top:522.00px" class="cls_011"><span class="cls_011">11</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background12.jpg" width=720 height=540></div>
<div style="position:absolute;left:87.19px;top:107.12px" class="cls_006"><span class="cls_006">Hierarchical and Network Models</span></div>
<div style="position:absolute;left:108.22px;top:182.36px" class="cls_018"><span class="cls_018">Hierarchical Models</span></div>
<div style="position:absolute;left:465.42px;top:182.36px" class="cls_018"><span class="cls_018">Network Models</span></div>
<div style="position:absolute;left:45.85px;top:215.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Manage large amounts of data</span></div>
<div style="position:absolute;left:387.30px;top:215.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Represent complex data</span></div>
<div style="position:absolute;left:66.00px;top:242.96px" class="cls_020"><span class="cls_020">for complex manufacturing</span></div>
<div style="position:absolute;left:407.45px;top:242.96px" class="cls_020"><span class="cls_020">relationships</span></div>
<div style="position:absolute;left:66.00px;top:268.88px" class="cls_020"><span class="cls_020">projects</span></div>
<div style="position:absolute;left:387.30px;top:277.76px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Improve database performance</span></div>
<div style="position:absolute;left:45.85px;top:303.92px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Represented by an upside-</span></div>
<div style="position:absolute;left:407.45px;top:303.92px" class="cls_020"><span class="cls_020">and impose a database</span></div>
<div style="position:absolute;left:66.00px;top:330.80px" class="cls_020"><span class="cls_020">down tree which contains</span></div>
<div style="position:absolute;left:407.45px;top:330.80px" class="cls_020"><span class="cls_020">standard</span></div>
<div style="position:absolute;left:66.00px;top:356.96px" class="cls_020"><span class="cls_020">segments</span></div>
<div style="position:absolute;left:387.30px;top:365.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Depicts both one-to-many</span></div>
<div style="position:absolute;left:69.60px;top:392.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_022"> Segments</span><span class="cls_023">: Equivalent of a file</span></div>
<div style="position:absolute;left:407.45px;top:392.96px" class="cls_020"><span class="cls_020">(1:M) and many-to-many</span></div>
<div style="position:absolute;left:89.05px;top:416.80px" class="cls_023"><span class="cls_023">system’s record type</span></div>
<div style="position:absolute;left:407.45px;top:418.88px" class="cls_020"><span class="cls_020">(M:N) relationships</span></div>
<div style="position:absolute;left:45.85px;top:449.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Depicts a set of one-to-many</span></div>
<div style="position:absolute;left:66.00px;top:475.76px" class="cls_020"><span class="cls_020">(1:M) relationships</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">12</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:6600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background13.jpg" width=720 height=540></div>
<div style="position:absolute;left:203.88px;top:107.12px" class="cls_006"><span class="cls_006">Hierarchical Model</span></div>
<div style="position:absolute;left:143.36px;top:182.36px" class="cls_018"><span class="cls_018">Advantages</span></div>
<div style="position:absolute;left:474.05px;top:182.36px" class="cls_018"><span class="cls_018">Disadvantages</span></div>
<div style="position:absolute;left:45.80px;top:215.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Promotes data sharing</span></div>
<div style="position:absolute;left:387.30px;top:215.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Requires knowledge of physical</span></div>
<div style="position:absolute;left:407.45px;top:239.92px" class="cls_023"><span class="cls_023">data storage characteristics</span></div>
<div style="position:absolute;left:45.80px;top:248.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Parent/child relationship promotes</span></div>
<div style="position:absolute;left:65.95px;top:272.80px" class="cls_023"><span class="cls_023">conceptual simplicity and data</span></div>
<div style="position:absolute;left:387.30px;top:272.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Navigational system requires</span></div>
<div style="position:absolute;left:65.95px;top:296.80px" class="cls_023"><span class="cls_023">integrity</span></div>
<div style="position:absolute;left:407.45px;top:296.80px" class="cls_023"><span class="cls_023">knowledge of hierarchical path</span></div>
<div style="position:absolute;left:45.80px;top:329.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Database security is provided and</span></div>
<div style="position:absolute;left:387.30px;top:329.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Changes in structure require</span></div>
<div style="position:absolute;left:65.95px;top:353.92px" class="cls_023"><span class="cls_023">enforced by DBMS</span></div>
<div style="position:absolute;left:407.45px;top:353.92px" class="cls_023"><span class="cls_023">changes in all application</span></div>
<div style="position:absolute;left:407.45px;top:377.92px" class="cls_023"><span class="cls_023">programs</span></div>
<div style="position:absolute;left:45.80px;top:386.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Efficient with 1:M relationships</span></div>
<div style="position:absolute;left:387.30px;top:410.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Implementation limitations</span></div>
<div style="position:absolute;left:387.30px;top:443.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> No data definition</span></div>
<div style="position:absolute;left:387.30px;top:476.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Lack of standards</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">13</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background14.jpg" width=720 height=540></div>
<div style="position:absolute;left:232.75px;top:107.12px" class="cls_006"><span class="cls_006">Network Model</span></div>
<div style="position:absolute;left:143.36px;top:182.36px" class="cls_018"><span class="cls_018">Advantages</span></div>
<div style="position:absolute;left:474.05px;top:182.36px" class="cls_018"><span class="cls_018">Disadvantages</span></div>
<div style="position:absolute;left:45.80px;top:215.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Conceptual simplicity</span></div>
<div style="position:absolute;left:387.30px;top:215.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> System complexity limits</span></div>
<div style="position:absolute;left:407.45px;top:239.92px" class="cls_023"><span class="cls_023">efficiency</span></div>
<div style="position:absolute;left:45.80px;top:248.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Handles more relationship types</span></div>
<div style="position:absolute;left:387.30px;top:272.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Navigational system yields</span></div>
<div style="position:absolute;left:45.80px;top:281.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Data access is flexible</span></div>
<div style="position:absolute;left:407.45px;top:296.80px" class="cls_023"><span class="cls_023">complex implementation,</span></div>
<div style="position:absolute;left:45.80px;top:314.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Data owner/member relationship</span></div>
<div style="position:absolute;left:407.45px;top:320.80px" class="cls_023"><span class="cls_023">application development, and</span></div>
<div style="position:absolute;left:65.95px;top:338.80px" class="cls_023"><span class="cls_023">promotes data integrity</span></div>
<div style="position:absolute;left:407.45px;top:344.80px" class="cls_023"><span class="cls_023">management</span></div>
<div style="position:absolute;left:45.80px;top:371.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Conformance to standards</span></div>
<div style="position:absolute;left:387.30px;top:377.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Structural changes require</span></div>
<div style="position:absolute;left:407.45px;top:401.92px" class="cls_023"><span class="cls_023">changes in all application</span></div>
<div style="position:absolute;left:45.80px;top:404.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Includes data definition language</span></div>
<div style="position:absolute;left:407.45px;top:425.92px" class="cls_023"><span class="cls_023">programs</span></div>
<div style="position:absolute;left:65.95px;top:428.80px" class="cls_023"><span class="cls_023">(DDL) and data manipulation</span></div>
<div style="position:absolute;left:65.95px;top:452.80px" class="cls_023"><span class="cls_023">language (DML)</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">14</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:7700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background15.jpg" width=720 height=540></div>
<div style="position:absolute;left:154.00px;top:76.80px" class="cls_017"><span class="cls_017">Standard Database Concepts</span></div>
<div style="position:absolute;left:54.42px;top:157.20px" class="cls_024"><span class="cls_024">Schema</span></div>
<div style="position:absolute;left:56.38px;top:218.64px" class="cls_025"><span class="cls_025">• Conceptual organization of the entire database as viewed by</span></div>
<div style="position:absolute;left:74.38px;top:242.64px" class="cls_025"><span class="cls_025">the database administrator</span></div>
<div style="position:absolute;left:54.42px;top:300.72px" class="cls_024"><span class="cls_024">Subschema</span></div>
<div style="position:absolute;left:56.38px;top:369.36px" class="cls_025"><span class="cls_025">• Portion of the database seen by the application programs that</span></div>
<div style="position:absolute;left:74.38px;top:393.36px" class="cls_025"><span class="cls_025">produce the desired information from the data within the</span></div>
<div style="position:absolute;left:74.38px;top:417.36px" class="cls_025"><span class="cls_025">database</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">15</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background16.jpg" width=720 height=540></div>
<div style="position:absolute;left:154.00px;top:76.80px" class="cls_017"><span class="cls_017">Standard Database Concepts</span></div>
<div style="position:absolute;left:75.00px;top:163.28px" class="cls_026"><span class="cls_026">Data manipulation language (DML)</span></div>
<div style="position:absolute;left:73.43px;top:238.92px" class="cls_027"><span class="cls_027">• Environment in which data can be managed and is</span></div>
<div style="position:absolute;left:91.43px;top:265.80px" class="cls_027"><span class="cls_027">used to work with the data in the database</span></div>
<div style="position:absolute;left:75.00px;top:329.36px" class="cls_026"><span class="cls_026">Schema data definition language</span></div>
<div style="position:absolute;left:75.00px;top:362.48px" class="cls_026"><span class="cls_026">(DDL)</span></div>
<div style="position:absolute;left:73.43px;top:431.16px" class="cls_027"><span class="cls_027">• Enables the database administrator to define the</span></div>
<div style="position:absolute;left:91.43px;top:458.28px" class="cls_027"><span class="cls_027">schema components</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">16</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:8800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background17.jpg" width=720 height=540></div>
<div style="position:absolute;left:184.50px;top:52.88px" class="cls_006"><span class="cls_006">The Relational Model</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Produced an automatic transmission database that</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_008"><span class="cls_008">replaced standard transmission databases</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Based on a relation</span></div>
<div style="position:absolute;left:75.55px;top:247.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_013"> Relation </span><span class="cls_010">or</span><span class="cls_013"> table</span><span class="cls_010">: Matrix composed of intersecting</span></div>
<div style="position:absolute;left:94.95px;top:278.56px" class="cls_010"><span class="cls_010">tuple and attribute</span></div>
<div style="position:absolute;left:98.60px;top:318.48px" class="cls_028"><span class="cls_028">§</span><span class="cls_029"> Tuple</span><span class="cls_025">: Rows</span></div>
<div style="position:absolute;left:98.60px;top:356.64px" class="cls_028"><span class="cls_028">§</span><span class="cls_029"> Attribute</span><span class="cls_025">: Columns</span></div>
<div style="position:absolute;left:51.80px;top:394.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Describes a precise set of data manipulation</span></div>
<div style="position:absolute;left:71.95px;top:427.52px" class="cls_008"><span class="cls_008">constructs</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">17</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background18.jpg" width=720 height=540></div>
<div style="position:absolute;left:220.56px;top:107.12px" class="cls_006"><span class="cls_006">Relational Model</span></div>
<div style="position:absolute;left:143.36px;top:182.36px" class="cls_018"><span class="cls_018">Advantages</span></div>
<div style="position:absolute;left:474.05px;top:182.36px" class="cls_018"><span class="cls_018">Disadvantages</span></div>
<div style="position:absolute;left:45.85px;top:213.76px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Structural independence is</span></div>
<div style="position:absolute;left:387.30px;top:215.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Requires substantial hardware and</span></div>
<div style="position:absolute;left:66.00px;top:235.84px" class="cls_023"><span class="cls_023">promoted using independent</span></div>
<div style="position:absolute;left:407.45px;top:239.92px" class="cls_023"><span class="cls_023">system software overhead</span></div>
<div style="position:absolute;left:66.00px;top:256.96px" class="cls_023"><span class="cls_023">tables</span></div>
<div style="position:absolute;left:387.30px;top:272.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Conceptual simplicity gives</span></div>
<div style="position:absolute;left:45.85px;top:287.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Tabular view improves</span></div>
<div style="position:absolute;left:407.45px;top:296.80px" class="cls_023"><span class="cls_023">untrained people the tools to use a</span></div>
<div style="position:absolute;left:66.00px;top:308.80px" class="cls_023"><span class="cls_023">conceptual simplicity</span></div>
<div style="position:absolute;left:407.45px;top:320.80px" class="cls_023"><span class="cls_023">good system poorly</span></div>
<div style="position:absolute;left:45.85px;top:339.76px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Ad hoc query capability is based</span></div>
<div style="position:absolute;left:387.30px;top:353.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> May promote information</span></div>
<div style="position:absolute;left:66.00px;top:361.84px" class="cls_023"><span class="cls_023">on SQL</span></div>
<div style="position:absolute;left:407.45px;top:377.92px" class="cls_023"><span class="cls_023">problems</span></div>
<div style="position:absolute;left:45.85px;top:391.84px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Isolates the end user from</span></div>
<div style="position:absolute;left:66.00px;top:413.92px" class="cls_023"><span class="cls_023">physical-level details</span></div>
<div style="position:absolute;left:45.85px;top:443.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Improves implementation and</span></div>
<div style="position:absolute;left:66.00px;top:465.76px" class="cls_023"><span class="cls_023">management simplicity</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">18</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:9900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background19.jpg" width=720 height=540></div>
<div style="position:absolute;left:117.00px;top:33.84px" class="cls_017"><span class="cls_017">Relational Database Management</span></div>
<div style="position:absolute;left:232.00px;top:76.80px" class="cls_017"><span class="cls_017">System(RDBMS)</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Performs basic functions provided by the hierarchical</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_008"><span class="cls_008">and network DBMS systems</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Makes the relational data model easier to understand</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_008"><span class="cls_008">and implement</span></div>
<div style="position:absolute;left:51.80px;top:280.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Hides the complexities of the relational model from</span></div>
<div style="position:absolute;left:71.95px;top:314.48px" class="cls_008"><span class="cls_008">the user</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">19</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:10450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background20.jpg" width=720 height=540></div>
<div style="position:absolute;left:86.64px;top:52.88px" class="cls_006"><span class="cls_006">Figure 2.2 - A Relational Diagram</span></div>
<div style="position:absolute;left:443.88px;top:477.44px" class="cls_030"><span class="cls_030">Cengage Learning © 2015</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">20</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background21.jpg" width=720 height=540></div>
<div style="position:absolute;left:127.00px;top:33.84px" class="cls_017"><span class="cls_017">SQL-Based Relational Database</span></div>
<div style="position:absolute;left:275.00px;top:76.80px" class="cls_017"><span class="cls_017">Application</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> End-user interface</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Allows end user to interact with the data</span></div>
<div style="position:absolute;left:51.80px;top:211.52px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Collection of tables stored in the database</span></div>
<div style="position:absolute;left:75.55px;top:254.56px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Each table is independent from another</span></div>
<div style="position:absolute;left:75.55px;top:294.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Rows in different tables are related based on common</span></div>
<div style="position:absolute;left:94.95px;top:325.60px" class="cls_010"><span class="cls_010">values in common attributes</span></div>
<div style="position:absolute;left:51.80px;top:365.60px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> SQL engine</span></div>
<div style="position:absolute;left:75.55px;top:408.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Executes all queries</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">21</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:11550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background22.jpg" width=720 height=540></div>
<div style="position:absolute;left:111.69px;top:52.88px" class="cls_006"><span class="cls_006">The Entity Relationship Model</span></div>
<div style="position:absolute;left:51.85px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Graphical representation of entities and their</span></div>
<div style="position:absolute;left:72.00px;top:162.56px" class="cls_008"><span class="cls_008">relationships in a database structure</span></div>
<div style="position:absolute;left:51.85px;top:204.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Entity relationship diagram (ERD)</span></div>
<div style="position:absolute;left:75.60px;top:247.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Uses graphic representations to model database</span></div>
<div style="position:absolute;left:95.05px;top:278.56px" class="cls_010"><span class="cls_010">components</span></div>
<div style="position:absolute;left:51.85px;top:318.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Entity instance or entity occurrence</span></div>
<div style="position:absolute;left:75.60px;top:361.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Rows in the relational table</span></div>
<div style="position:absolute;left:51.85px;top:401.60px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Connectivity</span><span class="cls_008">: Term used to label the relationship</span></div>
<div style="position:absolute;left:72.00px;top:435.68px" class="cls_008"><span class="cls_008">types</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">22</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background23.jpg" width=720 height=540></div>
<div style="position:absolute;left:147.75px;top:107.12px" class="cls_006"><span class="cls_006">Entity Relationship Model</span></div>
<div style="position:absolute;left:143.36px;top:182.36px" class="cls_018"><span class="cls_018">Advantages</span></div>
<div style="position:absolute;left:474.05px;top:182.36px" class="cls_018"><span class="cls_018">Disadvantages</span></div>
<div style="position:absolute;left:45.80px;top:215.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Visual modeling yields</span></div>
<div style="position:absolute;left:387.30px;top:215.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Limited constraint</span></div>
<div style="position:absolute;left:65.95px;top:242.96px" class="cls_020"><span class="cls_020">conceptual simplicity</span></div>
<div style="position:absolute;left:407.45px;top:242.96px" class="cls_020"><span class="cls_020">representation</span></div>
<div style="position:absolute;left:45.80px;top:277.76px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Visual representation makes it</span></div>
<div style="position:absolute;left:387.30px;top:277.76px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Limited relationship</span></div>
<div style="position:absolute;left:65.95px;top:303.92px" class="cls_020"><span class="cls_020">an effective communication</span></div>
<div style="position:absolute;left:407.45px;top:303.92px" class="cls_020"><span class="cls_020">representation</span></div>
<div style="position:absolute;left:65.95px;top:330.80px" class="cls_020"><span class="cls_020">tool</span></div>
<div style="position:absolute;left:387.30px;top:339.92px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> No data manipulation</span></div>
<div style="position:absolute;left:45.80px;top:365.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Is integrated with the dominant</span></div>
<div style="position:absolute;left:407.45px;top:365.84px" class="cls_020"><span class="cls_020">language</span></div>
<div style="position:absolute;left:65.95px;top:392.96px" class="cls_020"><span class="cls_020">relational model</span></div>
<div style="position:absolute;left:387.30px;top:401.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Loss of information content</span></div>
<div style="position:absolute;left:407.45px;top:427.76px" class="cls_020"><span class="cls_020">occurs when attributes are</span></div>
<div style="position:absolute;left:407.45px;top:453.92px" class="cls_020"><span class="cls_020">removed from entities to avoid</span></div>
<div style="position:absolute;left:407.45px;top:480.80px" class="cls_020"><span class="cls_020">crowded displays</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">23</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:12650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background24.jpg" width=720 height=540></div>
<div style="position:absolute;left:59.80px;top:52.88px" class="cls_006"><span class="cls_006">Figure 2.3 - The ER Model Notations</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">24</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background25.jpg" width=720 height=540></div>
<div style="position:absolute;left:49.00px;top:33.84px" class="cls_017"><span class="cls_017">The Object-Oriented Data Model (OODM)</span></div>
<div style="position:absolute;left:183.50px;top:76.80px" class="cls_017"><span class="cls_017">or Semantic Data Model</span></div>
<div style="position:absolute;left:51.85px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Object-oriented database management</span></div>
<div style="position:absolute;left:72.00px;top:162.56px" class="cls_014"><span class="cls_014">system(OODBMS)</span></div>
<div style="position:absolute;left:75.60px;top:204.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Based on OODM</span></div>
<div style="position:absolute;left:51.85px;top:244.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Object</span><span class="cls_008">: Contains data and their relationships with</span></div>
<div style="position:absolute;left:72.00px;top:278.48px" class="cls_008"><span class="cls_008">operations that are performed on it</span></div>
<div style="position:absolute;left:75.60px;top:321.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Basic building block for autonomous structures</span></div>
<div style="position:absolute;left:75.60px;top:361.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Abstraction of real-world entity</span></div>
<div style="position:absolute;left:51.85px;top:401.60px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Attributes - Describe the properties of an object</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">25</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:13750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background26.jpg" width=720 height=540></div>
<div style="position:absolute;left:49.00px;top:55.44px" class="cls_017"><span class="cls_017">The Object-Oriented Data Model (OODM)</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Class</span><span class="cls_008">: Collection of similar objects with shared</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_008"><span class="cls_008">structure and behavior organized in a class hierarchy</span></div>
<div style="position:absolute;left:75.55px;top:204.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_013"> Class hierarchy</span><span class="cls_010">: Resembles an upside-down tree in</span></div>
<div style="position:absolute;left:94.95px;top:236.56px" class="cls_010"><span class="cls_010">which each class has only one parent</span></div>
<div style="position:absolute;left:51.80px;top:276.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Inheritance</span><span class="cls_008">: Object inherits methods and attributes</span></div>
<div style="position:absolute;left:71.95px;top:309.68px" class="cls_008"><span class="cls_008">of parent class</span></div>
<div style="position:absolute;left:51.80px;top:352.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Unified Modeling Language</span><span class="cls_008"> (</span><span class="cls_014">UML)</span></div>
<div style="position:absolute;left:75.55px;top:395.68px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Describes sets of diagrams and symbols to graphically</span></div>
<div style="position:absolute;left:94.95px;top:426.64px" class="cls_010"><span class="cls_010">model a system</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">26</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14300px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background27.jpg" width=720 height=540></div>
<div style="position:absolute;left:172.75px;top:107.12px" class="cls_006"><span class="cls_006">Object-Oriented Model</span></div>
<div style="position:absolute;left:143.36px;top:182.36px" class="cls_018"><span class="cls_018">Advantages</span></div>
<div style="position:absolute;left:474.05px;top:182.36px" class="cls_018"><span class="cls_018">Disadvantages</span></div>
<div style="position:absolute;left:45.80px;top:215.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Semantic content is added</span></div>
<div style="position:absolute;left:387.35px;top:213.92px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Slow development of</span></div>
<div style="position:absolute;left:407.50px;top:236.96px" class="cls_020"><span class="cls_020">standards caused vendors to</span></div>
<div style="position:absolute;left:45.80px;top:251.84px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Visual representation includes</span></div>
<div style="position:absolute;left:407.50px;top:260.96px" class="cls_020"><span class="cls_020">supply their own</span></div>
<div style="position:absolute;left:65.95px;top:277.76px" class="cls_020"><span class="cls_020">semantic content</span></div>
<div style="position:absolute;left:407.50px;top:284.96px" class="cls_020"><span class="cls_020">enhancements</span></div>
<div style="position:absolute;left:45.80px;top:312.80px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Inheritance promotes data</span></div>
<div style="position:absolute;left:411.10px;top:317.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Compromised widely accepted</span></div>
<div style="position:absolute;left:65.95px;top:339.92px" class="cls_020"><span class="cls_020">integrity</span></div>
<div style="position:absolute;left:430.55px;top:339.76px" class="cls_023"><span class="cls_023">standard</span></div>
<div style="position:absolute;left:387.35px;top:369.92px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Complex navigational system</span></div>
<div style="position:absolute;left:387.35px;top:402.80px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> Learning curve is steep</span></div>
<div style="position:absolute;left:387.35px;top:434.96px" class="cls_019"><span class="cls_019">§</span><span class="cls_020"> High system overhead slows</span></div>
<div style="position:absolute;left:407.50px;top:458.96px" class="cls_020"><span class="cls_020">transactions</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">27</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:14850px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background28.jpg" width=720 height=540></div>
<div style="position:absolute;left:63.98px;top:33.84px" class="cls_017"><span class="cls_017">Figure 2.4 - A Comparison of OO, UML,</span></div>
<div style="position:absolute;left:248.00px;top:76.80px" class="cls_017"><span class="cls_017">and ER Models</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">28</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15400px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background29.jpg" width=720 height=540></div>
<div style="position:absolute;left:135.63px;top:52.88px" class="cls_006"><span class="cls_006">Object/Relational and XML</span></div>
<div style="position:absolute;left:69.85px;top:113.60px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Extended relational data model (ERDM)</span></div>
<div style="position:absolute;left:93.60px;top:152.56px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Supports OO features and complex data</span></div>
<div style="position:absolute;left:113.05px;top:180.64px" class="cls_010"><span class="cls_010">representation</span></div>
<div style="position:absolute;left:93.60px;top:217.60px" class="cls_009"><span class="cls_009">§</span><span class="cls_013"> Object/Relational Database Management System</span></div>
<div style="position:absolute;left:113.05px;top:245.68px" class="cls_013"><span class="cls_013">(O/R DBMS)</span></div>
<div style="position:absolute;left:116.65px;top:283.68px" class="cls_028"><span class="cls_028">§</span><span class="cls_025"> Based on ERDM, focuses on better data management</span></div>
<div style="position:absolute;left:69.85px;top:317.60px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Extensible Markup Language (XML)</span></div>
<div style="position:absolute;left:93.60px;top:357.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Manages unstructured data for efficient and</span></div>
<div style="position:absolute;left:113.05px;top:385.60px" class="cls_010"><span class="cls_010">effective exchange of all data types</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">29</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:15950px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background30.jpg" width=720 height=540></div>
<div style="position:absolute;left:288.37px;top:52.88px" class="cls_006"><span class="cls_006">Big Data</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Aims to:</span></div>
<div style="position:absolute;left:75.55px;top:171.52px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Find new and better ways to manage large amounts of</span></div>
<div style="position:absolute;left:94.95px;top:202.48px" class="cls_010"><span class="cls_010">web and sensor-generated data</span></div>
<div style="position:absolute;left:75.55px;top:242.56px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Provide high performance and scalability at a</span></div>
<div style="position:absolute;left:94.95px;top:273.52px" class="cls_010"><span class="cls_010">reasonable cost</span></div>
<div style="position:absolute;left:51.80px;top:313.52px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Characteristics</span></div>
<div style="position:absolute;left:75.55px;top:356.56px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Volume</span></div>
<div style="position:absolute;left:75.55px;top:396.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Velocity</span></div>
<div style="position:absolute;left:75.55px;top:437.68px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Variety</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">30</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:16500px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background31.jpg" width=720 height=540></div>
<div style="position:absolute;left:194.50px;top:52.88px" class="cls_006"><span class="cls_006">Big Data Challenges</span></div>
<div style="position:absolute;left:112.07px;top:133.64px" class="cls_031"><span class="cls_031">Volume does not allow the usage of</span></div>
<div style="position:absolute;left:112.07px;top:164.60px" class="cls_031"><span class="cls_031">conventional structures</span></div>
<div style="position:absolute;left:112.07px;top:267.80px" class="cls_031"><span class="cls_031">Expensive</span></div>
<div style="position:absolute;left:112.07px;top:371.00px" class="cls_031"><span class="cls_031">OLAP tools proved inconsistent dealing</span></div>
<div style="position:absolute;left:112.07px;top:401.96px" class="cls_031"><span class="cls_031">with unstructured data</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">31</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17050px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background32.jpg" width=720 height=540></div>
<div style="position:absolute;left:134.63px;top:52.88px" class="cls_006"><span class="cls_006">Big Data New Technologies</span></div>
<div style="position:absolute;left:396.24px;top:167.84px" class="cls_032"><span class="cls_032">Hadoop Distributed</span></div>
<div style="position:absolute;left:168.89px;top:181.76px" class="cls_032"><span class="cls_032">Hadoop</span></div>
<div style="position:absolute;left:396.66px;top:195.92px" class="cls_032"><span class="cls_032">File System (HDFS)</span></div>
<div style="position:absolute;left:144.03px;top:372.32px" class="cls_032"><span class="cls_032">MapReduce</span></div>
<div style="position:absolute;left:470.52px;top:372.32px" class="cls_032"><span class="cls_032">NoSQL</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">32</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:17600px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background33.jpg" width=720 height=540></div>
<div style="position:absolute;left:212.43px;top:52.88px" class="cls_006"><span class="cls_006">NoSQL Databases</span></div>
<div style="position:absolute;left:51.85px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Not based on the relational model</span></div>
<div style="position:absolute;left:51.85px;top:171.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Support distributed database architectures</span></div>
<div style="position:absolute;left:51.85px;top:213.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Provide high scalability, high availability, and fault</span></div>
<div style="position:absolute;left:72.00px;top:247.52px" class="cls_008"><span class="cls_008">tolerance</span></div>
<div style="position:absolute;left:51.85px;top:289.52px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Support large amounts of sparse data</span></div>
<div style="position:absolute;left:51.85px;top:332.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Geared toward performance rather than transaction</span></div>
<div style="position:absolute;left:72.00px;top:366.56px" class="cls_008"><span class="cls_008">consistency</span></div>
<div style="position:absolute;left:51.85px;top:408.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Store data in key-value stores</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">33</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18150px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background34.jpg" width=720 height=540></div>
<div style="position:absolute;left:297.81px;top:107.12px" class="cls_006"><span class="cls_006">NoSQL</span></div>
<div style="position:absolute;left:143.36px;top:182.36px" class="cls_018"><span class="cls_018">Advantages</span></div>
<div style="position:absolute;left:474.05px;top:182.36px" class="cls_018"><span class="cls_018">Disadvantages</span></div>
<div style="position:absolute;left:45.80px;top:215.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> High scalability, availability, and</span></div>
<div style="position:absolute;left:387.30px;top:215.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Complex programming is</span></div>
<div style="position:absolute;left:65.95px;top:239.92px" class="cls_023"><span class="cls_023">fault tolerance are provided</span></div>
<div style="position:absolute;left:407.45px;top:239.92px" class="cls_023"><span class="cls_023">required</span></div>
<div style="position:absolute;left:45.80px;top:272.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Uses low-cost commodity</span></div>
<div style="position:absolute;left:387.30px;top:272.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> There is no relationship support</span></div>
<div style="position:absolute;left:65.95px;top:296.80px" class="cls_023"><span class="cls_023">hardware</span></div>
<div style="position:absolute;left:387.30px;top:305.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> There is no transaction integrity</span></div>
<div style="position:absolute;left:45.80px;top:329.92px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> Supports Big Data</span></div>
<div style="position:absolute;left:407.45px;top:329.92px" class="cls_023"><span class="cls_023">support</span></div>
<div style="position:absolute;left:45.80px;top:362.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_023"> 4. Key-value model improves</span></div>
<div style="position:absolute;left:387.30px;top:362.80px" class="cls_021"><span class="cls_021">§</span><span class="cls_033"> In terms of data consistency, it</span></div>
<div style="position:absolute;left:65.95px;top:386.80px" class="cls_023"><span class="cls_023">storage efficiency</span></div>
<div style="position:absolute;left:407.45px;top:386.80px" class="cls_033"><span class="cls_033">provides an eventually consistent</span></div>
<div style="position:absolute;left:407.45px;top:410.80px" class="cls_033"><span class="cls_033">model</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">34</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:18700px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background35.jpg" width=720 height=540></div>
<div style="position:absolute;left:124.99px;top:33.84px" class="cls_017"><span class="cls_017">Figure 2.5 - A Simple Key-value</span></div>
<div style="position:absolute;left:252.00px;top:76.80px" class="cls_017"><span class="cls_017">Representation</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">35</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19250px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background36.jpg" width=720 height=540></div>
<div style="position:absolute;left:51.32px;top:55.44px" class="cls_017"><span class="cls_017">Figure 2.6 - The Evolution of Data Models</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">36</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:19800px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background37.jpg" width=720 height=540></div>
<div style="position:absolute;left:51.34px;top:33.60px" class="cls_017"><span class="cls_017">Table 2.3 - Data Model Basic Terminology</span></div>
<div style="position:absolute;left:272.00px;top:76.56px" class="cls_017"><span class="cls_017">Comparison</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">37</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20350px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background38.jpg" width=720 height=540></div>
<div style="position:absolute;left:67.73px;top:52.88px" class="cls_006"><span class="cls_006">Figure 2.7 - Data Abstraction Levels</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">38</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:20900px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background39.jpg" width=720 height=540></div>
<div style="position:absolute;left:198.94px;top:52.88px" class="cls_006"><span class="cls_006">The External Model</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> End users’ view of the data environment</span></div>
<div style="position:absolute;left:51.80px;top:171.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> ER diagrams are used to represent the external views</span></div>
<div style="position:absolute;left:51.80px;top:213.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> External schema</span><span class="cls_008">: Specific representation of an</span></div>
<div style="position:absolute;left:71.95px;top:247.52px" class="cls_008"><span class="cls_008">external view</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">39</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:21450px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background40.jpg" width=720 height=540></div>
<div style="position:absolute;left:88.46px;top:33.84px" class="cls_017"><span class="cls_017">Figure 2.8 - External Models for Tiny</span></div>
<div style="position:absolute;left:304.00px;top:76.80px" class="cls_017"><span class="cls_017">College</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">40</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22000px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background41.jpg" width=720 height=540></div>
<div style="position:absolute;left:175.62px;top:52.88px" class="cls_006"><span class="cls_006">The Conceptual Model</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Represents a global view of the entire database by the</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_008"><span class="cls_008">entire organization</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Conceptual schema</span><span class="cls_008">: Basis for the identification and</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_008"><span class="cls_008">high-level description of the main data objects</span></div>
<div style="position:absolute;left:51.80px;top:280.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Has a macro-level view of data environment</span></div>
<div style="position:absolute;left:51.80px;top:323.60px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Is software and hardware independent</span></div>
<div style="position:absolute;left:51.80px;top:366.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Logical design</span><span class="cls_008">: Task of creating a conceptual data</span></div>
<div style="position:absolute;left:71.95px;top:399.68px" class="cls_008"><span class="cls_008">model</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">41</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:22550px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background42.jpg" width=720 height=540></div>
<div style="position:absolute;left:74.46px;top:33.84px" class="cls_017"><span class="cls_017">Figure 2.9 - Conceptual Model for Tiny</span></div>
<div style="position:absolute;left:304.00px;top:76.80px" class="cls_017"><span class="cls_017">College</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">42</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23100px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background43.jpg" width=720 height=540></div>
<div style="position:absolute;left:204.44px;top:52.88px" class="cls_006"><span class="cls_006">The Internal Model</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Representing database as seen by the DBMS</span></div>
<div style="position:absolute;left:71.95px;top:162.56px" class="cls_008"><span class="cls_008">mapping conceptual model to the DBMS</span></div>
<div style="position:absolute;left:51.80px;top:204.56px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Internal schema</span><span class="cls_008">: Specific representation of an</span></div>
<div style="position:absolute;left:71.95px;top:238.64px" class="cls_008"><span class="cls_008">internal model</span></div>
<div style="position:absolute;left:75.55px;top:281.68px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Uses the database constructs supported by the chosen</span></div>
<div style="position:absolute;left:94.95px;top:312.64px" class="cls_010"><span class="cls_010">database</span></div>
<div style="position:absolute;left:51.80px;top:352.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Is software dependent and hardware independent</span></div>
<div style="position:absolute;left:51.80px;top:394.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Logical independence</span><span class="cls_008">: Changing internal model</span></div>
<div style="position:absolute;left:71.95px;top:428.48px" class="cls_008"><span class="cls_008">without affecting the conceptual model</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">43</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:23650px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background44.jpg" width=720 height=540></div>
<div style="position:absolute;left:91.46px;top:33.84px" class="cls_017"><span class="cls_017">Figure 2.10 - Internal Model for Tiny</span></div>
<div style="position:absolute;left:304.00px;top:76.80px" class="cls_017"><span class="cls_017">College</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">44</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24200px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background45.jpg" width=720 height=540></div>
<div style="position:absolute;left:198.87px;top:52.88px" class="cls_006"><span class="cls_006">The Physical Model</span></div>
<div style="position:absolute;left:51.80px;top:128.48px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Operates at lowest level of abstraction</span></div>
<div style="position:absolute;left:51.80px;top:171.68px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Describes the way data are saved on storage media</span></div>
<div style="position:absolute;left:71.95px;top:204.56px" class="cls_008"><span class="cls_008">such as disks or tapes</span></div>
<div style="position:absolute;left:51.80px;top:247.52px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Requires the definition of physical storage and data</span></div>
<div style="position:absolute;left:71.95px;top:280.64px" class="cls_008"><span class="cls_008">access methods</span></div>
<div style="position:absolute;left:51.80px;top:323.60px" class="cls_007"><span class="cls_007">§</span><span class="cls_008"> Relational model aimed at logical level</span></div>
<div style="position:absolute;left:75.55px;top:366.64px" class="cls_009"><span class="cls_009">§</span><span class="cls_010"> Does not require physical-level details</span></div>
<div style="position:absolute;left:51.80px;top:406.64px" class="cls_007"><span class="cls_007">§</span><span class="cls_014"> Physical independence</span><span class="cls_008">: Changes in physical model</span></div>
<div style="position:absolute;left:71.95px;top:439.52px" class="cls_008"><span class="cls_008">do not affect internal model</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">45</span></div>
</div>
<div style="position:absolute;left:50%;margin-left:-360px;top:24750px;width:720px;height:540px;border-style:outset;overflow:hidden">
<div style="position:absolute;left:0px;top:0px">
<img src="lecture2/background46.jpg" width=720 height=540></div>
<div style="position:absolute;left:54.19px;top:52.88px" class="cls_006"><span class="cls_006">Table 2.4 - Levels of Data Abstraction</span></div>
<div style="position:absolute;left:547.13px;top:405.44px" class="cls_030"><span class="cls_030">Cengage Learning © 2015</span></div>
<div style="position:absolute;left:83.31px;top:524.52px" class="cls_005"><span class="cls_005">©2015 Cengage Learning. All Rights Reserved. May not be scanned, copied or duplicated, or posted to a publicly accessible website, in whole or in part.</span></div>
<div style="position:absolute;left:699.45px;top:522.00px" class="cls_011"><span class="cls_011">46</span></div>
</div>

</body>
</html>
