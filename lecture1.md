## Quiz        

1. The data is ubiquitous and pervasive
    - ubiquitous
        - abundant, global, everywhere
    - pervasive
        - unescapable, prevalent, persistent

2. The databases are specialized structures that allow computer-based systems to store, manage, and retrieve data very quickly

3. The **data** consists of **raw** facts
    - The word **raw** indicates that the facts have **not** yet been processed to reveal their **meaning**
    - The foundation of information 
    - The building blocks of information

4. The **information** is the result of processing raw data to reveal its **meaning**
    - The context
    - The bedrock of knowledge
    - It is produced by processing data
    - It is used to reveal the meaning of data
    - It is accurate, relevant, and timely. They are the key to good decision making
    - Good decision making is the key to organizational survival

5. Data management 
    - generation 
    - storage 
    - retrieval

6. Introducing the Database
    - It is stored, shared, and integrated computer structure
        - End-user data 
            - raw facts of interest to end user
        - Metadata 
            - data about data
            - the end-user data is integrated and managed
            - the data characteristics and relationships

7. The database management system (DBMS)
    - collection of programs and software
        - create data dictionary
    - manages the database structure 
    - controls access to the data stored in the database

8. The role and Advantages of the DBMS
    - **enables** the **data** in the database to be **shared** among multiple applications or users
    - **integrates** the many different users’ **views** of the **data** into a single all-encompassing data repository
        - logical **view** of data
    - make data management more **efficient** and **effective**
    - Improved data **sharing**
    - Improved data **security**
    - Better data **integration**
    - Minimized data **inconsistency**
    - Improved data **access**
        - query
    - Improved **decision making**
        - Data quality is a comprehensive approach to promoting the accuracy, validity, and timeliness of the data 
        - The DBMS does not guarantee data quality

9. Types of Databases number of users
    - Single-user
        - supports only one user at a time
        - The **desktop database** is the single-user database that runs on a **personal computer**
    - Multiuser
        - supports multiple users at the same time
        - The **workgroup database** is the multiuser database supports multiple users at the same time. When the multiuser database supports a relatively small number of users (usually fewer than 50) or a **specific department** within an organization 
        - The **enterprise database** is the database is used by the entire organization and supports many users (more than 50, usually hundreds) across **many departments**

10. Types of Databases location
    - Centralized database
        - data located at a **single site**
    - Distributed database
        - data distributed **across several different sites**
    - cloud database
        - created and maintained using cloud data services that provide defined performance measures (data storage capacity, required throughput, and availability) for the database

11. Types of Databases data type
    - General- purpose databases
        - contain a wide variety of data used in **multiple disciplines**
    - Discipline-specific databases
        - contain data focused on **specific subject areas**
    - operational database transaction database production database
        - A database that is designed primarily to support a company’s **day-to-day** operation
    - analytical database
        - A database that focuses primarily on storing **historical** data and business metrics used exclusively for tactical or strategic **decision making**
        - The **data warehouse** is a specialized database that **stores data in a format optimized for decision support**
        - The **online analytical processing (OLAP)** is a set of **tools** that work together to provide an advanced data analysis environment for **retrieving, processing, and modeling data from the data warehouse**
        - The term **business intelligence** describes a comprehensive approach to **capture and process business data with the purpose of generating information to support business decision making**

12. Types of Databases structured and unstructured
    - Databases can also be classified to reflect the degree to which the data is structured
    - **Unstructured** data is data that exists in its **original** (raw) stat
    - **Structured data is the result of formatting** unstructured data to facilitate storage, use, and generation of information
        - You apply **structure** (format) **based on the type of processing** that you intend to **perform** on the data  
    - **Semistructured data** has already been **processed to some extent**
    - **Extensible Markup Language (XML)** is a special language used to **represent and manipulate data elements in a textual format**
        - An XML database supports the **storage and management** of semistructured XML data
            - it store and manage database on the web
    - The term NoSQL (Not only SQL) is generally used to describe a new generation of DBMS that is not based on the traditional relational database model

13. Why Database Design Is Important 
    - Database design refers to the activities that focus on the design of the database structure that will be used to store and manage end-user data
    - Well-designed database facilitates data management and generates accurate and valuable information
    - Poorly designed database causes difficult-to-trace errors that may lead to poor decision making

14. Evolution of file System Data Processing
    - Manual file systems
        - accomplished through a system of file folders and filing cabinets
    - Computerized file systems
        - the data processing (DP) specialist was hired to create a computer-based system that would track data and produce required reports
    - File System Redux
        - the spreadsheet programs such as Microsoft Excel
    - field is called attribute, record is called row, file is called table

15. Problems with file System Data Processing
    - challenge the types of information that can be created from the data as well as the accuracy of the information
        - Lengthy development times
        - Difficulty of getting quick answers
        - Complex system administration
        - Lack of security and limited data sharing
        - Extensive programming











    











